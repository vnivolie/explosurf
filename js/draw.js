(function(factory) {
  //namespacing
  if(!window["Explosurf"]) {
    window["Explosurf"] = {} ;
  }
  if(!window["Explosurf"]["draw"]) {
    window["Explosurf"]["draw"] = {} ;
  }
  factory(window["Explosurf"]["draw"]) ;
})(function(Draw) { //namespace Explosurf.draw

/* local storage prefix */
var storage_prefix = 'Explosurf:' 
    + document.getElementById('planet-name').innerHTML.toLowerCase().replace(/\s+/mg, '')
    + ':draw'
  ;

var area = document.getElementById("draw-area") ;
var ctx = area.getContext("2d") ;
var center_img = document.getElementById("tile_center") ;
var cursor = document.createElement('img') ;
cursor.src = window.location.href.split('/').slice(0, -2).join('/') + '/Images/bonhomme.png' ;

var pad = {
  tiles : {},

  xmax : 0,
  xmin : 0,
  ymax : 0,
  ymin : 0,

  pen : {
    x : 0,
    y : 0,
  },
}

function local_save() {
  var data = JSON.stringify(
      pad, 
      function(key, value) {
        if(key === 'tiles') {
          var paths = {} ;
          for(var entry in value) {
            var fname = value[entry].src.split('/').pop() ;
            paths[entry] = fname ;
          }
          return paths ;
        }
        return value ;
      }) ;
  localStorage.setItem(storage_prefix, data) ;
}

function loading_image(path) {
  var img = new Image() ;
  img.loading = true ;
  img.onload = function() {img.loading = false ;} ;
  img.src = path ;
  return img ;
}

function local_load() {
  var data = JSON.parse(
      localStorage.getItem(storage_prefix),
      function(key, value) {
        if(key === 'tiles') {
          var imgs = {} ;
          for(var entry in value) {
            var path = window.location.href.replace(/#$/,'') + value[entry] ;
            imgs[entry] = loading_image(path) ;
          }
          return imgs ;
        }
        return value ;
      }) ;
  if(data) {
    pad = data ;
  }
}

function recompute_minmax() {
  var initialized = false ;
  for(var key in pad.tiles) {
    var pen = JSON.parse(key) ;
    if(! initialized) {
      pad.xmin = pen.x ;
      pad.xmax = pen.x ;
      pad.ymin = pen.y ;
      pad.ymax = pen.y ;
      initialized = true ;
    } else {
      pad.xmax = pad.xmax < pen.x ? pen.x : pad.xmax ;
      pad.xmin = pad.xmin > pen.x ? pen.x : pad.xmin ;
      pad.ymax = pad.ymax < pen.y ? pen.y : pad.ymax ;
      pad.ymin = pad.ymin > pen.y ? pen.y : pad.ymin ;
    }
  }
}

async function draw(img, x, y, sx, sy, other) {
  await img.decode() ;
  if(other) {
    await other.decode() ;
  }
  ctx.drawImage(img, x, y, sx, sy) ;
}

async function redraw() {
  ctx.clearRect(0,0,area.width, area.height) ;

  var xmin = pad.pen.x < pad.xmin ? pad.pen.x : pad.xmin ;
  var xmax = pad.pen.x > pad.xmax ? pad.pen.x : pad.xmax ;
  var ymin = pad.pen.y < pad.ymin ? pad.pen.y : pad.ymin ;
  var ymax = pad.pen.y > pad.ymax ? pad.pen.y : pad.ymax ;

  var wx = xmax - xmin + 1;
  var wy = ymax - ymin + 1;

  var sx = Math.floor(area.width / wx) ;
  var sy = Math.floor(area.height / wy) ;

  var s = sx < sy ? sx : sy ;
  s = s > 256 ? 256 : s ;

  var ox = Math.floor(area.width / 2 - (xmin + wx / 2)*s)  ;
  var oy = Math.floor(area.height / 2 - (ymin + wy / 2)*s)  ;

  for(var key in pad.tiles) {
    var img = pad.tiles[key] ;
    var pen = JSON.parse(key) ;
    await draw(img, ox + pen.x * s, oy + pen.y * s, s, s) ;
  }
  
  var cs = s / 3 ;
  var cursor_tile = pad.tiles[JSON.stringify(pad.pen)] ;
  await draw(cursor, ox + pad.pen.x * s + cs, oy + pad.pen.y * s + cs, cs, cs, cursor_tile) ;

  local_save() ;
}

Draw.redraw = redraw ;

function stamp() {
  var key = JSON.stringify(pad.pen) ;
  pad.tiles[key] = loading_image(center_img.src) ;

  pad.xmax = pad.xmax < pad.pen.x ? pad.pen.x : pad.xmax ;
  pad.xmin = pad.xmin > pad.pen.x ? pad.pen.x : pad.xmin ;
  pad.ymax = pad.ymax < pad.pen.y ? pad.pen.y : pad.ymax ;
  pad.ymin = pad.ymin > pad.pen.y ? pad.pen.y : pad.ymin ;
}

Draw.stamp = stamp ;

function clear() {
  var key = JSON.stringify(pad.pen) ;
  delete pad.tiles[key] ;
  recompute_minmax() ;
}

Draw.clear = clear ;

function clear_all() {
  pad.tiles = {} ;
  pad.xmin = 0 ;
  pad.xmax = 0 ;
  pad.ymin = 0 ;
  pad.ymax = 0 ;
  pad.pen.x = 0 ;
  pad.pen.y = 0 ;
}

Draw.clear_all = clear_all ;

function go(direction) {
    if(direction === 0) {
      pad.pen.y += 1 ;
    } else if(direction === 1) {
      pad.pen.x += 1 ;
    } else if(direction === 2) {
      pad.pen.y -= 1 ;
    } else if(direction === 3) {
      pad.pen.x -= 1 ;
    }
}

Draw.go = go ;

document.getElementById('draw-down').addEventListener(
    'click',
    function() {go(0) ; redraw() ;}
    ) ;
document.getElementById('draw-right').addEventListener(
    'click',
    function() {go(1) ; redraw() ;}
    ) ;
document.getElementById('draw-up').addEventListener(
    'click',
    function() {go(2) ; redraw() ;}
    ) ;
document.getElementById('draw-left').addEventListener(
    'click',
    function() {go(3) ; redraw() ;}
    ) ;

document.getElementById('draw-clear').addEventListener(
    'click',
    function() {clear() ; redraw() ;}
    ) ;
document.getElementById('draw-clear-all').addEventListener(
    'click',
    function() {clear_all() ; redraw() ;}
    ) ;
document.getElementById('draw-stamp').addEventListener(
    'click',
    function() {stamp() ; redraw() ;}
    ) ;

document.addEventListener("DOMContentLoaded", function(event) {
  local_load() ;
  redraw() ;
}) ;

})//end of namespace Explosurf.draw

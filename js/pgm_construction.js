(function(factory) {
  //namespacing
  if(!window["Explosurf"]) {
    window["Explosurf"] = {} ;
  }
  if(!window["Explosurf"]["pgm"]) {
    window["Explosurf"]["pgm"] = {} ;
  }
  factory(window["Explosurf"]["pgm"]) ;
})(function(Pgm) { //namespace Explosurf.pgm

var library = document.getElementById('library') ;
var library_panel = document.getElementById('library-panel') ;
var pgm = document.getElementById('pgm') ;
var pgm_container = document.getElementById('program') ;
var main = document.getElementById('pgm-main') ;
var phone_toolbar = document.getElementById('phone-toolbar') ;

/* {{{ General tools ======================================================== */

function addClass(el,c) {
  var re = new RegExp("(?:^|\\s)" + c + "(?!\\S)") ;
  if(!el.className.match(re)) { 
    el.className += ' ' + c ; 
  }
}

Pgm.addClass = addClass ;

function removeClass(el,c) {
  var re = new RegExp("(?:^|\\s)" + c + "(?!\\S)") ;
  el.className = el.className.replace( re , '' ) ;
}

Pgm.removeClass = removeClass ;

var has_absolute_fixed_bug = (function() {
  var container = document.createElement('div') ;
  container.style.position = 'relative' ;
  container.style.left = '0px' ;

  var child = document.createElement('div') ;
  child.style.position = 'fixed' ;

  container.appendChild(child) ;
  document.body.appendChild(container) ;

  child.getBoundingClientRect() ;
  container.style.left = '20px' ;

  var child_left = child.getBoundingClientRect().left ;
  var container_left = container.getBoundingClientRect().left ;
  document.body.removeChild(container) ;

  return child_left !== container_left;
})() ;

/*}}}*/

/* {{{ Load and save ======================================================== */

/*prefix for local storage*/
var storage_prefix = 'Explosurf:' 
    + document.getElementById('planet-name').innerHTML.toLowerCase().replace(/\s+/mg, '')
    + ':pgm'
  ;

/*clear current program*/
function pgm_clear() {
  var adder = main.getElementsByClassName('block-add')[0] ;
  main.innerHTML = '' ;
  main.appendChild(adder) ;
}

document.getElementById('pgm-clear').addEventListener(
  'click',
  pgm_clear
) ;

/* save a program as default for the planet or with the given name */
function local_save(name) {
  var prefix = typeof(name) === 'string' && name ? name : storage_prefix ;
  var inputs = main.getElementsByTagName('input') ;
  for(var i = 0; i < inputs.length; ++i) {
    inputs[i].setAttribute('value', inputs[i].value) ;
  }
  localStorage.setItem(prefix, main.innerHTML) ;
}

Pgm.save = local_save ;

window.addEventListener('beforeunload', local_save) ;

/* callback for the save program modal */
function local_save_as() {
  var name_elt = document.getElementById('pgm-save-name') ;
  if(name_elt.value) {
    local_save('Explosurf:all:pgm:' + name_elt.value) ;
  }
}

document.getElementById('pgm-save-local').addEventListener(
  'click',
  local_save_as
) ;

/* load the program saved for the planet or that with the given name */
function local_load(name) {
  var prefix = typeof(name) === 'string' && name ? name : storage_prefix ;
  var program = localStorage.getItem(prefix) ;
  if(program) {
    main.innerHTML = program ;
    update_pgm_ui() ;
  }
}

/* template for the list of saved programs */
var pgm_entry = (function() {
  var pgm_entry = document.createElement('a') ;
  pgm_entry.className = 'list-group-item clearfix' ;

  var trash_btn = document.createElement('button') ;
  trash_btn.className = 'btn btn-link pull-right' ;
  trash_btn.style.padding = 0 ;
  pgm_entry.appendChild(trash_btn) ;

  var trash_icon = document.createElement('span') ;
  trash_icon.className = 'glyphicon glyphicon-trash' ;
  trash_btn.appendChild(trash_icon) ;

  return pgm_entry
})() ;

/* build the list of saved programs in the modal */
function local_list_pgm() {
  var pgm_list = document.getElementById('pgm-load-candidates') ;
  var pgm_no_pgm = document.getElementById('pgm-no-pgm') ;
  //reset the list
  pgm_list.innerHTML = '' ;
  //hide the no program info panel
  pgm_no_pgm.style.display = 'none' ;
  var items = 0 ;
  //list saved programs
  for(key in localStorage) {
    var match = key.match(/^Explosurf:all:pgm:(.+)$/) ;
    if(match) {
      ++items ;
      //build the item from the template
      var item = pgm_entry.cloneNode(true) ;
      var label = document.createElement('span') ;
      label.innerHTML = match[1] ;
      label.className = 'pull-left' ;
      item.appendChild(label) ;
      pgm_list.appendChild(item) ;

      //hook the corresponding events
      item.addEventListener('click', local_list_select) ;
      item.children[0].addEventListener('click', pgm_delete_local(match[1])) ;
    }
  }
  //show the no program panel if no program found
  if(!items) {
    pgm_no_pgm.style.display = '' ;
  }
}

document.getElementById('pgm-load').addEventListener(
  'click',
  local_list_pgm
) ;

/* callback for toggling the currently selected program for loading */
function local_list_select(evt) {
  var pgm_list = document.getElementById('pgm-load-candidates') ;
  var elt = evt.target ;
  for(var i = 0; i < pgm_list.children.length; ++i) {
    var item = pgm_list.children[i] ;
    if(item == elt) {
      addClass(item, 'active') ;
    } else {
      removeClass(item, 'active') ;
    }
  }
}

/* callback generator for saved program deletion */
function pgm_delete_local(name) {
  return function() {
    localStorage.removeItem('Explosurf:all:pgm:' + name) ;
    local_list_pgm() ;
  }
}

/* saved program loading */
document.getElementById('pgm-load-local').addEventListener(
  'click',
  function(evt) {
    var pgm_list = document.getElementById('pgm-load-candidates') ;
    for(var i = 0; i < pgm_list.children.length; ++i) {
      var item = pgm_list.children[i] ;
      if(item.className.match(/active/)) {
        var name = item.children[1].innerHTML ;
        local_load('Explosurf:all:pgm:' + name) ;
      }
    }
  }
) ;

/* save all data using filesaver.js */
function blob_download() {
  local_save() ;
  var blob = new Blob(
    [JSON.stringify(localStorage)],
    {type : 'application/json'}
  ) ;
  saveAs(blob, 'explosurf.json') ;
}

/* load the saved data */
function load_file(evt) {
  var files = evt.target.files ;
  var file = null ;
  if(files) {
    file = files[0] ;
  }
  if(file) {
    //clear previous localStorage entries
    for(key in localStorage) {
      if(key.match(/^Explosurf/)) {
        localStorage.removeItem(key) ;
      }
    }
    //load new localStorage entries
    var reader = new FileReader() ;
    reader.onload = function(e) {
      var data = JSON.parse(e.target.result) ;
      for(key in data) {
        localStorage.setItem(key, data[key]) ;
      }
      local_load() ;
      Explosurf.notes.load() ;
      Explosurf.explorer.load() ;
    }
    reader.readAsText(file) ;
  }
}

/* full data save and load hooks */
document.getElementById("save-btn").addEventListener(
  'click',
  blob_download
) ;

document.getElementById("file-open").addEventListener(
  'change',
  load_file
) ;

/*}}}*/

/* {{{ Desktop interface ==================================================== */

/* On desktop browser the program construction is handled using drag and drop.
 * */

/* type groups */
var pgm_type_groups = [
  "pgm-type-void",
  "pgm-type-color", 
  "pgm-type-direction", 
  "pgm-type-bool", 
  "pgm-type-int", 
  "pgm-type-str", 
  "pgm-type-read"
] ;

/* a block is a list of commands. It can only receive void commands (no return
 * type) or colors and directions for moving.
 */

function desktop_activate_block(el) {
  if(!el.sortable_obj) {
    var sortable_obj = Sortable.create(
        el,
        {
          group : {
            //setup received elements
            put : [
              "pgm-type-void", 
              "pgm-type-color", 
              "pgm-type-direction", 
              "pgm-type-read"
            ]
          },
          animation : 0,
          filter : ".no-sort",
          //activate sortables on drop
          onAdd : activate_sortables_callback,
          //setup element groups from their class on drag start
          onStart : setup_group_callback,
          //reactivate deactivated elements while dragged
          onEnd : activate_sortables_callback,
          //activate cloned elements after copy
          onUpdate : activate_sortables_callback,
        }
        ) ;
    //store the sortable in the DOM
    el.sortable_obj = sortable_obj ;
  } else {
    el.sortable_obj.options.disabled = false ;
  }
}

/* inputs are disabled for elements that are in the library to prevent the user
 * from filling the inputs while this should only happen for elements in the
 * program.
 */

function enable_input(el) {
  //remove the disabled attribute from the html element
  el.removeAttribute("disabled") ;
  //fix because inputs in draggables are hardly focusable
  el.addEventListener("mousedown", function(evt) { evt.target.focus() ;}) ;
}

/* a receiver is inline in a command. It is generally a string for variable
 * names integers for arithmetic operations, booleans for tests.
 */

function desktop_activate_receiver(el) {
  if(!el.sortable_obj) {
    //deduce the desired type from the class name
    var groupName = 
      el.className.match(/pgm-recv-\S*/)[0].replace("recv", "type") ;
    var groups = [] ;
    if(groupName === "pgm-type-str") {
      groups = [
        "pgm-type-str", 
        "pgm-type-int", 
        "pgm-type-bool", 
        "pgm-type-color", 
        "pgm-type-direction", 
        "pgm-type-read"
      ]
    } else {
      groups = [groupName, "pgm-type-read"]
    }
    //list the inputs in the element (normally only one) and activate them
    var pgmInputs = el.getElementsByTagName('input') ;
    [].forEach.call(pgmInputs, enable_input) ;
    //create the sortable
    var sortable_obj = Sortable.create(
        el,
        {
          group : {
            //setup received elements
            put : el.children.length == 1 ? groups : false,
            save_put : groups,
          },
          sort : false,
          animation : 0,
          filter : ".no-sort, input, .placeholder",
          //deal with the original input, and disable putting when filled
          onAdd : placeholder_add_callback,
          //setup group when element taken out
          onStart : setup_group_callback,
          //reactivate deactivated elements while dragged
          onEnd : activate_sortables_callback,
          //reenable putting when element removed
          onRemove : placeholder_remove_callback,
          //activate cloned elements after copy
          onUpdate : activate_sortables_callback,
        }
        ) ;
    //store the sortable in the DOM
    el.sortable_obj = sortable_obj ;
  } else {
    el.sortable_obj.options.disabled = false ;
  }
}

/* placeholders have either an input or a dummy div describing what should be
 * filled. for inputs it can be manually filled. Upon drop, the previous element
 * has to disappear, and no further element should be added.
 */

function placeholder_add_callback(evt) {
  var el = evt.item ;
  var receiver = evt.target ;
  //save previous group settings
  receiver.sortable_obj.options.group.save_put = 
    receiver.sortable_obj.options.group.put ;
  //prevent adding another element
  receiver.sortable_obj.options.group.put = false ;
  //activate placed item
  activate_sortables(el) ;
}

function placeholder_remove_callback(evt) {
  var el = evt.item ;
  var source = evt.from ;
  //check that the removal is not a cloned removal
  if(source.children.length == 1) {
    //restore the ability to add elements
    source.sortable_obj.options.group.put = 
      source.sortable_obj.options.group.save_put ;
  }
}

function console_callback(evt) {
  console.log("called") ;
}

/* activate all the sortable descendants of an element. This is used when an
 * element is dropped to activate its sortable parts disabled while in the
 * library.
 */

function activate_sortables(el) {
  //list blocks and activate them
  var pgm_blocks = el.getElementsByClassName('pgm-recv-void') ;
  [].forEach.call(pgm_blocks, desktop_activate_block) ;
  //list receivers and activate them
  var pgm_receivers = el.getElementsByClassName('pgm-recv') ;
  [].forEach.call(pgm_receivers, desktop_activate_receiver) ;
}

function deactivate_sortables(el) {
  //list blocks and activate them
  var pgm_blocks = el.getElementsByClassName('pgm-recv-void') ;
  [].forEach.call(pgm_blocks, deactivate_sortable) ;
  //list receivers and activate them
  var pgm_receivers = el.getElementsByClassName('pgm-recv') ;
  [].forEach.call(pgm_receivers, deactivate_sortable) ;
}

/* copy mode : dragged objects are cloned rather than moved */

function activate_cloning(el) {
  if(el.sortable_obj) {
    el.sortable_obj.options.group.pull = "sort-clone" ;
  }
}

function deactivate_cloning(el) {
  if(el.sortable_obj) {
    el.sortable_obj.options.group.pull = true ;
  }
}

function activate_copy_mode() {
  //list blocks and activate them
  var pgm_blocks = pgm.getElementsByClassName('pgm-recv-void') ;
  [].forEach.call(pgm_blocks, activate_cloning) ;
  //list receivers and activate them
  var pgm_receivers = pgm.getElementsByClassName('pgm-recv') ;
  [].forEach.call(pgm_receivers, activate_cloning) ;
}

function deactivate_copy_mode() {
  //list blocks and activate them
  var pgm_blocks = pgm.getElementsByClassName('pgm-recv-void') ;
  [].forEach.call(pgm_blocks, deactivate_cloning) ;
  //list receivers and activate them
  var pgm_receivers = pgm.getElementsByClassName('pgm-recv') ;
  [].forEach.call(pgm_receivers, deactivate_cloning) ;
}

function toggle_copy() {
  var copy_switch = document.getElementById("desktop-ui-copy") ;
  if(copy_switch.className.match(/active/)) {
    removeClass(copy_switch, "active") ;
    deactivate_copy_mode() ;
  } else {
    addClass(copy_switch, "active") ;
    activate_copy_mode() ;
  }
}

document.getElementById("desktop-ui-copy").addEventListener(
  'click',
  function(evt) {
    toggle_copy() ;
  }
) ;

function copy_detect(evt){
  if(document.getElementsByClassName("sortable-ghost").length == 0) {
    var evtobj=window.event? event : evt
    if(evtobj.key === "Control") {
      toggle_copy() ;
    }
  }
}

document.addEventListener("keydown", copy_detect) ;
document.addEventListener("keyup", copy_detect) ;

function activate_sortables_callback(evt) {
  var el = evt.item ;
  activate_sortables(el) ;
}

/* the library is where elements are taken from. It is not possible to mess with
 * the order of its elements or to remove elements : when dragged, an element is
 * cloned.
 */

function library_fixed_switch() {
  pgm_rect = pgm.getBoundingClientRect() ;
  lib_rect = library_panel.getBoundingClientRect() ;
  if(pgm_rect.top < 10) {
    if(pgm_rect.bottom > lib_rect.height + 10) {
      addClass(library_panel, 'fixed-top') ;
      removeClass(pgm_container, 'align-bottom') ;
    } else {
      addClass(pgm_container, 'align-bottom') ;
      removeClass(library_panel, 'fixed-top') ;
    }
  } else {
    removeClass(pgm_container, 'align-bottom') ;
    removeClass(library_panel, 'fixed-top') ;
  }
}

function pin_library() {
  var lib_style = window.getComputedStyle(library) ;
  var w = parseInt(lib_style.getPropertyValue('width').replace(/px/, '')) ;
  var pl = parseInt(lib_style.getPropertyValue('padding-left').replace(/px/, '')) ;
  var pr = parseInt(lib_style.getPropertyValue('padding-right').replace(/px/, '')) ;
  library_panel.style.width = (w - pl - pr) + 'px' ;
  document.body.addEventListener('scroll', library_fixed_switch) ;
}

function unpin_library() {
  library_panel.style.width = '' ;
  document.body.removeEventListener('scroll', library_fixed_switch) ;
}

function desktop_activate_library() {
  //reset the library placement
  library.style.top = '' ;
  //locate the library groups in the DOM and activate them
  var libraryGroups = document.getElementsByClassName("library-group") ;
  [].forEach.call(libraryGroups, function (el) {
    if(!el.sortable) {
      var sortable_obj = Sortable.create(
          el,
          {
            group : {
              //elements should be cloned rather than removed
              pull : "clone",
              //any element can be dropped for trash
              put : pgm_type_groups
            },
            ghostClass : 'library-ghost',
            //the element order cannot be changed
            sort : false,
            //setup group when element taken out
            onStart : setup_group_callback,
            //drop elements added : act as trash
            onAdd : trash_callback

          }
          ) ;
      //store the sortable in the DOM
      el.sortable_obj = sortable_obj ;
    } else {
      el.sortable_obj.options.disabled = false ;
    }
  }) ;
  pin_library() ;
}

function desktop_deactivate_library() {
  //locate the library groups in the DOM and activate them
  var libraryGroups = document.getElementsByClassName("library-group") ;
  [].forEach.call(libraryGroups, function (el) {
    deactivate_sortable(el) ;
  }) ;
  unpin_library() ;
}

/* the group of an element corresponds to its return type. It is described by
 * a class added to the element in the DOM with the model "pgm-type-xxx" where
 * xxx can be direction, color, int, bool, str or void. We use this type as the
 * group of the dragged elements, so that the sortables can choose to receive
 * only compatible elements.
 */

function setup_group_callback(evt) {
  //derive the group name from the class name
  var groupName = evt.item.className.match(/pgm-type-[a-z]*/)[0] ;
  //no clue why the group name is part of this but it works
  this.options.group.name = groupName ;
  //activate the clone
  deactivate_sortables(evt.item) ;
}

/* trashing an element means removing it right after addition */
function trash_callback(evt) {
  var container = evt.item.parentElement ;
  container.removeChild(evt.item) ;
}

/* activate the main program which is the only initial receiver
 */

function deactivate_sortable(el) {
  if(el.sortable_obj) {
    el.sortable_obj.options.disabled = true ;
  }
}

function desktop_activate_program() {
  activate_sortables(main.parentElement) ;
}

function desktop_deactivate_program() {
  deactivate_sortables(main.parentElement) ;
}

function desktop_activate() {
  desktop_activate_program() ;
  desktop_activate_library() ;
}

function desktop_deactivate() {
  desktop_deactivate_program() ;
  desktop_deactivate_library() ;
}

/*}}}*/

/* {{{ Mobile interface ===================================================== */

/* On mobile devices, drag and drop is difficult, so the program construction is
 * handled via clicks, a global context and various states
 */

/* The context. Keys assigned :
 *   * element : copy/cut element or that before which an insertion should occur
 *   * container : element in which the insertion should occur
 *   * active_input : currently focused input, another click triggers addition
 *   * last_action : last click action
 */

var pgm_context = {} ;

/* Checks */

/* check whether a void command can be added above the clocked element */
function check_addable(el) {
  var in_block = el.parentElement.className.match(/pgm-recv-void/) ;
  return in_block || !el.className.match(/pgm-command/) ;
}

/* check whether the element can be copied or cut */
function check_copycutable(el) {
  return !el.className.match(/block-add/)
    && !el.className.match(/placeholder/)
    && !el.tagName.match(/INPUT/) ;
}

/* type of elements a receiver can receive */
function container_type(container) {
  return container.className.match(/recv-\S*/)[0].replace("recv-", "") ;
}

/* check at paste that the pasted element is compatible with the container */
function check_types(el, container) {
  var type = container_type(container) ;
  if(type === "void") {
    return el.className.match(/type-void|type-color|type-direction|type-read/) ;
  } else if(type == "str") {
    return !el.className.match(/type-void/) ;
  } else {
    var re = new RegExp("pgm-type-" + type + "|pgm-type-read") ;
    return el.className.match(re) ;
  }
}

/* click handling */
function mobile_pgm_click(evt) {
  //reset active input
  pgm_context.active_input = undefined ;
  //check tool and redirect accordingly
  var state = phone_tools_get() ;
  if(state === 'phone-tool-cut') return pgm_cut(evt) ;
  if(state === 'phone-tool-add') return pgm_add(evt) ;
  if(state === 'phone-tool-copy') return pgm_copy(evt) ;
  if(state === 'phone-tool-paste') return pgm_paste(evt) ;
}

/* addition : mark the element as target, open and filter the library */
function pgm_add(evt) {
  var el = evt.currentTarget ;
  //check whether addition can occur above / inside
  if(check_addable(el)) {
    //reset context (remove other styling)
    mobile_context_reset() ;
    pgm_context.last_action = "add" ;
    //style the element
    addClass(el, "add-target") ;
    //store the element and its container in the context
    if(el.tagName === 'INPUT') {
      el = el.parentElement ;
    }
    var container = el.parentElement ;
    pgm_context.element = el ;
    pgm_context.container = container ;
    //filter the library according to the desired type
    addClass(library, "filter-" + container_type(container)) ;
    //open the library
    open_library() ;
    //do not handle the event further up
    evt.stopPropagation() ;
  }
}

/* copy : mark the element as source, store it for pasting, switch to paste */
function pgm_copy(evt) {
  //check whether the element can be copied
  var el = evt.currentTarget ;
  if(check_copycutable(el)) {
    //reset context (remove other styling)
    mobile_context_reset() ;
    pgm_context.last_action = "copycut" ;
    //style the element
    addClass(el, "copy-source") ;
    //store the element in the context for potential pasting
    pgm_context.element = el ;
    pgm_context.container = undefined ;
    //switch to paste
    phone_tools_set("phone-tool-paste") ;
    //do not handle the event further up
    evt.stopPropagation() ;
  }
}

/* copy : remove the element, store it for pasting, switch to paste */
function pgm_cut(evt) {
  //check whether the element can be cut
  var el = evt.currentTarget ;
  if(check_copycutable(el)) {
    //reset context (remove other styling)
    mobile_context_reset() ;
    pgm_context.last_action = "copycut" ;
    //store the element in the context for potential pasting
    pgm_context.element = el ;
    //remove the element from the program
    el.parentElement.removeChild(el) ;
    pgm_context.container = undefined ;
    //switch to paste
    phone_tools_set("phone-tool-paste") ;
    //do not handle the event further up
    evt.stopPropagation() ;
  }
}

/* paste : check types, clone the element stored, add above clicked element */
function pgm_paste(evt) {
  //check whether something is available for pasting
  if(pgm_context.element && pgm_context.last_action == "copycut") {
    //target for pasting
    var before_elt = evt.currentTarget ;
    //container for pasting
    var container = before_elt.parentElement ;
    //check whether the available element is compatible with the container
    if(!check_types(pgm_context.element, container)) return ;
    //clone the element to paste
    var clone_elt = pgm_context.element.cloneNode(true) ;
    removeClass(clone_elt, "copy-source") ;
    //insert it in the container
    before_elt.parentElement.insertBefore(clone_elt, before_elt) ;
    //configure it to receive clicks
    clone_elt.addEventListener("click", mobile_pgm_click) ;
    mobile_activate_blocks(clone_elt) ;
    //switch to add
    phone_tools_set("phone-tool-add") ;
    //do not handle the event further up
    evt.stopPropagation() ;
  }
}

/* reset context : no element marked, library filters removed */
function mobile_context_reset() {
  //remove styling for the addition target
  var targets = document.getElementsByClassName("add-target") ;
  while(targets.length) {
    removeClass(targets[0], "add-target") ;
  }
  //remove styling for the copy source
  var sources = document.getElementsByClassName("copy-source") ;
  while(sources.length) {
    removeClass(sources[0], "copy-source") ;
  }
  //remove library filters
  removeClass(library, "filter-\\S*") ;
  //reset context
  pgm_context.element = undefined ;
  pgm_context.container = undefined ;
  //close the library if ongoing addition
  close_library() ;
}

document.getElementById('btn-close-library').addEventListener(
  'click', 
  function(evt) {
    mobile_context_reset() ;
  }
) ;

/* Insert the chosen element and activate its events */
function insert_chosen(evt) {
  //check whether an addition is ongoing
  if(pgm_context.container && pgm_context.last_action == "add") {
    //should not be necessary since the library is filtered
    if(!check_types(evt.currentTarget, pgm_context.container)) return ;
    //clone the element from the library
    var clone_elt = evt.currentTarget.cloneNode(true) ;
    //insert it
    pgm_context.container.insertBefore(clone_elt, pgm_context.element) ;
    //activate it
    clone_elt.addEventListener("click", mobile_pgm_click) ;
    mobile_activate_blocks(clone_elt) ;
    //reset the context (remove addition styling)
    mobile_context_reset() ;
  }
}

/* Library activation : all items are clickable for addition */
function mobile_activate_library() {
  //reset the library placement
  library.style.top = '' ;
  //the commands are clickable for addition
  var library_cmds = library.getElementsByClassName("pgm-command") ;
  for(var i = 0; i < library_cmds.length; ++i) {
    library_cmds[i].addEventListener("click", insert_chosen) ;
  }
  //the blocks are clickable for addition
  var library_blocks = library.getElementsByClassName("pgm-block") ;
  for(var i = 0; i < library_blocks.length; ++i) {
    library_blocks[i].addEventListener("click", insert_chosen) ;
  }
  if(is_mobile_forced()) {
    pin_library() ;
  }
}

function mobile_deactivate_library() {
  //get the library element
  //deactivate the commands
  var library_cmds = library.getElementsByClassName("pgm-command") ;
  for(var i = 0; i < library_cmds.length; ++i) {
    library_cmds[i].removeEventListener("click", insert_chosen) ;
  }
  //deactivate the blocks
  var library_blocks = library.getElementsByClassName("pgm-block") ;
  for(var i = 0; i < library_blocks.length; ++i) {
    library_blocks[i].removeEventListener("click", insert_chosen) ;
  }
  unpin_library() ;
}

/* Input click : first click focuses, second triggers addition */
function input_click(evt) {
  //check whether the input is already focused
  if(evt.target != pgm_context.active_input) {
    //not focused, store in the contxt for next click
    pgm_context.active_input = evt.target ;
  } else {
    //already focused, blur to hide keyboard on mobiles
    evt.target.blur() ;
    //handle click for addition / copy / cut
    mobile_pgm_click(evt) ;
  }
  //do not handle the event further up
  evt.stopPropagation() ;
}

/* Element activation on mobiles : setup clickable elements*/
function mobile_activate_blocks(el) {
  //list pgm-blocks and activate them
  var pgm_blocks = el.getElementsByClassName("pgm-block") ;
  for(var i = 0; i < pgm_blocks.length; ++i) {
    pgm_blocks[i].addEventListener("click", mobile_pgm_click) ;
  }
  //list pgm-commands and activate them
  var pgm_commands = el.getElementsByClassName('pgm-command') ;
  for(var i = 0; i < pgm_commands.length; ++i) {
    pgm_commands[i].addEventListener("click", mobile_pgm_click) ;
  }
  //list the inputs in the element (normally only one) and activate them
  var pgm_inputs = el.getElementsByTagName('input') ;
  for(var i = 0; i < pgm_inputs.length; ++i) {
    pgm_inputs[i].removeAttribute("disabled") ;
    pgm_inputs[i].addEventListener("click", input_click) ;
  }
  //list the placeholders in the element (normally only one) and activate them
  var pgm_placeholders = el.getElementsByClassName('placeholder') ;
  for(var i = 0; i < pgm_placeholders.length; ++i) {
    pgm_placeholders[i].addEventListener("click", mobile_pgm_click) ;
  }
}

function mobile_deactivate_blocks(el) {
  var pgm_blocks = el.getElementsByClassName("pgm-block") ;
  //list pgm-blocks and deactivate them
  for(var i = 0; i < pgm_blocks.length; ++i) {
    pgm_blocks[i].removeEventListener("click", mobile_pgm_click) ;
  }
  //list pgm-commands and deactivate them
  var pgm_commands = el.getElementsByClassName('pgm-command') ;
  for(var i = 0; i < pgm_commands.length; ++i) {
    //commands inside receivers are not to be deactivated
    if(pgm_commands[i].parentElement.className.match(/pgm-recv-void/)) {
      pgm_commands[i].removeEventListener("click", mobile_pgm_click) ;
    }
  }
  //list the inputs in the element (normally only one) and activate them
  var pgm_inputs = el.getElementsByTagName('input') ;
  for(var i = 0; i < pgm_inputs.length; ++i) {
    pgm_inputs[i].removeEventListener("click", input_click) ;
  }
  //list the placeholders in the element (normally only one) and activate them
  var pgm_placeholders = el.getElementsByClassName('placeholder') ;
  for(var i = 0; i < pgm_placeholders.length; ++i) {
    pgm_placeholders[i].removeEventListener("click", mobile_pgm_click) ;
  }
}

function show_mobile_ui() {
  removeClass(phone_toolbar, "hidden") ;
  var library_close = document.getElementById("btn-close-library") ;
  removeClass(library_close, "hidden") ;
  var block_adders_collection = document.getElementsByClassName('block-add') ;
  //necessary, since it seems like appendChild messes with HTMLCollection
  var block_adders = [].slice.call(block_adders_collection) ;
  for(var i = 0; i < block_adders.length; ++i) {
    var el = block_adders[i] ;
    var el_parent = el.parentElement ;
    removeClass(el, "hidden") ;
    //replace adder at the end of the list
    el_parent.appendChild(el) ;
  } ;
}

function hide_mobile_ui() {
  addClass(phone_toolbar, "hidden") ;
  var library_close = document.getElementById("btn-close-library") ;
  addClass(library_close, "hidden") ;
  var block_adders = document.getElementsByClassName('block-add') ;
  [].forEach.call(block_adders, function(el) {
    addClass(el, "hidden") ;
  }) ;
}

function toggle_mobile() {
  var mobile_switch = document.getElementById("desktop-ui-phone") ;
  if(mobile_switch.className.match(/active/)) {
    removeClass(mobile_switch, "active") ;
    update_pgm_ui() ;
  } else {
    addClass(mobile_switch, "active") ;
    update_pgm_ui() ;
  }
}

document.getElementById("desktop-ui-phone").addEventListener(
  'click',
  function(evt) {
    toggle_mobile() ;
  }
) ;

function is_mobile() {
  var mobile_switch = document.getElementById("desktop-ui-phone") ;
  if(mobile_switch.className.match(/active/)) {
    return true ;
  }
  var test_el = document.getElementById("mobile-tester") ;
  return getComputedStyle(test_el ,null).display !== "none" ;
}

function is_mobile_forced() {
  var mobile_switch = document.getElementById("desktop-ui-phone") ;
  return mobile_switch.className.match(/active/)
}

function phone_tools_set(id) {
  var btns = phone_toolbar.getElementsByTagName("button") ;
  for(var i = 0; i < btns.length; ++i) {
    if(btns[i].id == id) {
      addClass(btns[i], "active") ;
    } else {
      removeClass(btns[i], "active") ;
    }
  }
}

document.getElementById('phone-tool-add').addEventListener(
  'click',
  function(evt) {
    phone_tools_set('phone-tool-add') ;
  }
) ;

document.getElementById('phone-tool-cut').addEventListener(
  'click',
  function(evt) {
    phone_tools_set('phone-tool-cut') ;
  }
) ;

document.getElementById('phone-tool-copy').addEventListener(
  'click',
  function(evt) {
    phone_tools_set('phone-tool-copy') ;
  }
) ;

document.getElementById('phone-tool-paste').addEventListener(
  'click',
  function(evt) {
    phone_tools_set('phone-tool-paste') ;
  }
) ;

function phone_tools_get() {
  var btns = phone_toolbar.getElementsByTagName("button") ;
  for(var i = 0; i < btns.length; ++i) {
    if(btns[i].className.match(/active/)) {
      return btns[i].id ;
    }
  }
}

function phone_tools_update() {
  if(has_absolute_fixed_bug) {
    var child = phone_toolbar.children[0] ;
    if(child) {
      phone_toolbar.removeChild(child) ;
      setTimeout(function() {phone_toolbar.appendChild(child);}, 250) ;
    }
  }
}

function phone_tools_fixed_switch() {
  if(phone_toolbar.children[0]) {
    if(pgm.getBoundingClientRect().top < 10) {
      addClass(phone_toolbar.children[0], 'fixed-bottom') ;
    } else {
      removeClass(phone_toolbar.children[0], 'fixed-bottom') ;
    }
  } else {
    //defer, because of collapse animation
    //errors only in browser web dev mode
    setTimeout(phone_tools_fixed_switch, 100) ;
  }
}

function pin_phone_toolbar() {
  document.body.addEventListener('scroll', phone_tools_fixed_switch) ;
}

function unpin_phone_toolbar() {
  document.body.removeEventListener('scroll', phone_tools_fixed_switch) ;
}

var toolbar_right = 0 ;

function open_library() {
  if(!is_mobile_forced()) {
    var elem = document.getElementById("program") ;
    addClass(elem, "active") ;
    var test_el = document.getElementById("mobile-tester") ;
    var scroll = pgm.getBoundingClientRect().top ;
    var topval = Math.max(0, 10 - scroll) ;
    library.style.top = topval + 'px' ;
    phone_tools_update() ;
  }
}

function close_library() {
  if(!is_mobile_forced()) {
    var elem = document.getElementById("program") ;
    removeClass(elem, "active") ;
    phone_tools_update() ;
  }
}

function mobile_activate_program() {
  mobile_activate_blocks(main) ;
}

function mobile_deactivate_program() {
  mobile_deactivate_blocks(main) ;
}

function mobile_activate() {
  mobile_activate_library() ;
  mobile_activate_program() ;
  show_mobile_ui() ;
  pin_phone_toolbar() ;
  phone_tools_fixed_switch() ;
}

function mobile_deactivate() {
  mobile_context_reset() ;
  mobile_deactivate_library() ;
  mobile_deactivate_program() ;
  hide_mobile_ui() ;
  unpin_phone_toolbar() ;
}

/*}}}*/

/* {{{ Activation =========================================================== */

function update_pgm_ui() {
  //check the display type : mobile or desktop
  if(is_mobile()) {
    //mobile browser
    desktop_deactivate() ;
    mobile_activate() ;
  } else {
    //desktop browser
    mobile_deactivate() ;
    desktop_activate() ;
  }
}

document.addEventListener("DOMContentLoaded", function(event) {
  local_load() ;
  update_pgm_ui() ;
});

window.addEventListener("resize", function(event) {
  update_pgm_ui() ;
});

/*}}}*/

});

(function(factory) {
  //namespacing
  if(!window["Explosurf"]) {
    window["Explosurf"] = {} ;
  }
  if(!window["Explosurf"]["notes"]) {
    window["Explosurf"]["notes"] = {} ;
  }
  factory(window["Explosurf"]["notes"]) ;
})(function(Notes) { //namespace Explosurf.notes

/* local storage prefix */
var storage_prefix = 'Explosurf:' 
    + document.getElementById('planet-name').innerHTML.toLowerCase().replace(/\s+/mg, '')
    + ':notes'
  ;

/* save notes in local storage */
Notes.save = function() {
  //tile_data
  var local_key = 
    document.getElementById("tile_center").src.match(/map\d+/) ;
  var local_notes = 
    document.getElementById("local_notes").value ;
  //get the data from the web page
  var global_notes = 
    document.getElementById("global_notes").value ;
  //save tile data
  localStorage.setItem(
      storage_prefix + ":local_notes:" + local_key,
      local_notes
  ) ;
  //save world data
  localStorage.setItem(
      storage_prefix + ":global_notes",
      global_notes
  ) ;
}

/* ensure saving when notes are changed */
document.getElementById("local_notes").addEventListener(
    'input',
    Notes.save
);
document.getElementById("global_notes").addEventListener(
    'input',
    Notes.save
);

window.addEventListener('beforeunload', Notes.sate) ;

/* load notes from local storage */
Notes.load = function() {
  //tile key
  var local_key = 
    document.getElementById("tile_center").src.match(/map\d+/) ;
  //get tile data
  var local_notes = localStorage.getItem(
      storage_prefix + ":local_notes:" + local_key
  ) ;
  //render local notes
  var local_target = document.getElementById("local_notes") ;
  if(local_notes) {
    local_target.value = local_notes ;
  } else {
    local_target.value = "" ;
  }
  //get world data
  var global_notes = localStorage.getItem(
      storage_prefix + ":global_notes"
  ) ;
  var global_target = document.getElementById("global_notes") ;
  if(global_notes) {
    global_target.value = global_notes ;
  } else {
    global_target.value = "" ;
  }
}

/* clear local notes */
Notes.clear_local = function() {
  for(key in localStorage) {
    if(key.match(storage_prefix + ":local_notes:")) {
      localStorage.removeItem(key) ;
    }
  }
  document.getElementById("local_notes").value = "" ;
}

document.getElementById("notes-local-delete").addEventListener(
    'click',
    Notes.clear_local
);

/* clear global notes */
Notes.clear_global = function() {
  localStorage.removeItem(storage_prefix + ":global_notes") ;
  document.getElementById("global_notes").value = "" ;
}

document.getElementById("notes-global-delete").addEventListener(
    'click',
    Notes.clear_global
);

/* set a variable. Set global to true for global variables */
Notes.set_variable = function(name, value, global) {
  var re = new RegExp("(\\[ " + name + " = )([^\\[\\]=]*)( \\])", "g") ;
  var notes_elt = global ?
    document.getElementById("global_notes") :
    document.getElementById("local_notes") ;
  if(notes_elt.value.match(re)) {
    notes_elt.value = notes_elt.value.replace(re, "$1" + value + "$3") ;
  } else {
    var prefix = notes_elt.value.match(/\S+$/) ? "\n" : "" ;
    notes_elt.value = notes_elt.value + prefix + "[ " + name + " = " + value + " ]" ;
  }
  Notes.save() ;
}

/* get a variable. Set global to true for global variables */
Notes.get_variable = function(name, global) {
  var re = new RegExp("\\[ " + name + " = ([^\\[\\]=]*) \\]") ;
  var notes_elt = global ?
    document.getElementById("global_notes") :
    document.getElementById("local_notes") ;
  var res = notes_elt.value.match(re) ;
  return res ? res[1] : res ;
}

/* get a variable. Set global to true for global variables */
Notes.check_variable = function(name, global) {
  var re = new RegExp("\\[ " + name + " = ([^\\[\\]=]*) \\]") ;
  var notes_elt = global ?
    document.getElementById("global_notes") :
    document.getElementById("local_notes") ;
  return notes_elt.value.match(re) !== null ;
}

/* clear a variable locally or globally */
Notes.clear_variable = function(name, global) {
  var re = new RegExp("\\[ " + name + " = ([^\\[\\]=]*) \\]\n?", "g") ;
  var notes_elt = global ?
    document.getElementById("global_notes") :
    document.getElementById("local_notes") ;
  notes_elt.value = notes_elt.value.replace(re, "") ;
  Notes.save() ;
}

/* clear local variables */
Notes.clear_local_variables = function(name) {
  var re = new RegExp("\\[ " + name + " = ([^\\[\\]=]*) \\]\n?", "g") ;
  for(key in localStorage) {
    if(key.match(storage_prefix + ":local_notes:")) {
      localStorage[key] = localStorage[key].replace(re, "") ;
    }
  }
  Notes.load() ;
}

}) //end of namespace

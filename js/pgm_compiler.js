(function(factory) {
  //namespacing
  if(!window["Explosurf"]) {
    window["Explosurf"] = {} ;
  }
  if(!window["Explosurf"]["pgm"]) {
    window["Explosurf"]["pgm"] = {} ;
  }
  factory(window["Explosurf"]["pgm"]) ;
})(function(Pgm) { //namespace Explosurf.pgm

var pgm_elements = {} ;

/* {{{ General tools ======================================================== */

function cmd_compile(el) {
  var cmd_name = el.className.match(/cmd-\S+/)[0] ;
  return pgm_elements[cmd_name](el) ;
}

function block_compile(el) {
  var res = '' ;
  for(var i = 0; i < el.children.length; ++i) {
    if(!el.children[i].className.match(/block-add/)) {
      res += cmd_compile(el.children[i]) ;
      res += ';\n' ;
    }
  }
  return res ;
}

function get_integer(data, error_target) {
  var str = typeof data === 'string' ? data : '' + data ;
  var i = parseInt(str) ;
  if(!str.match(/^-?\d+$/) || isNaN(i)) {
    return pgm_selector_error(
        error_target,
        "la valeur fournie n\'est pas un entier : " + str
        ) ;
  } else {
    return i ;
  }
}

function translate_bool(b) {
  return b ? '"vrai"' : '"faux"' ;
}

function get_boolean(data, error_target) {
  if(typeof data === 'boolean') {
    return data ;
  } else if(data.match(/vrai|faux/)) {
    return data === 'vrai' ;
  } else {
    return pgm_selector_error( 
        error_target ,
        "la valeur fournie n\'est ni vrai ni faux : " + data
        ) ;
  }
}

function get_str(data, error_target) {
  var str = typeof data === 'string' ? data : '' + data ;
  if(str.match(/^[\w\u00C0-\u02B8\-]+$/)) {
    return str ;
  } else {
    return pgm_selector_error(
        error_target,
        "seules les lettre, les chiffres et _ sont autorisés : " + str 
        );
  }
}

function get_msg(data, error_target) {
  var str = typeof data === 'string' ? data : '' + data ;
  if(str.match(/^[\w\u00C0-\u02B8\- ]+$/)) {
    return str ;
  } else {
    return pgm_selector_error(
        error_target,
        "seules les lettre, les chiffres, l'espace et _ sont autorisés : " + str 
        );
  }
}

function receiver_compile(el) {
  //type of the receiver
  var type = el.className.match(/pgm-recv-(\S+)/)[1] ;
  //search for the child to compile
  var target = null ;
  for(var i = 0; i < el.children.length; ++i) {
    var child = el.children[i] ;
    if(child.className.match(/placeholder|field/)) {
      //input or placeholder : use only if no other child is present
      if(!target) {
        target = child ;
      }
    } else {
      //a nested child was provided
      target = child ;
    }
  }
  //check whether a placeholder is filled
  if(target.className.match(/placeholder/)) {
    return pgm_error(target, "l'emplacement n'a pas été rempli") ;
  }
  //compile child
  var rec_compile = "" ;
  if(target.className.match(/field/)) {
    var err_target = selector_path_to_root(target.children[0]) ;
    if(type === 'str') {
      rec_compile = '"' + get_str(target.children[0].value, err_target) + '"' ;
    } else if(type === 'int') {
      rec_compile = get_integer(target.children[0].value, err_target)
    } else {
      rec_compile = '"' + get_msg(target.children[0].value, err_target) + '"' ;
    }
  }
  else {
    rec_compile = cmd_compile(target) ;
  }
  //handle runtime type errors
  var err_target = '"' + selector_path_to_root(target) + '"' ;
  if(type === 'str') {
    return 'get_str(' + rec_compile + ',' + err_target + ')' ;
  } else if(type === 'int') {
    return 'get_integer(' + rec_compile + ',' + err_target + ')' ;
  } else if (type === 'bool') {
    return 'get_boolean(' + rec_compile + ',' + err_target + ')' ;
  } else if (type === 'str' || type === 'msg') {
    return rec_compile ;
  } else if (type === 'direction') {
    return 'check_direction(' + rec_compile + ',' + err_target + ')'
  } else if (type === 'color') {
    return 'check_color(' + rec_compile + ',' + err_target + ')'
  } else {
    console.log("unable to match receiver type") ;
    console.log(el) ;
    return "__error__" ;
  }
}

function selector_path_to_root(el) {
  if(el.id === "pgm-main") {
    return '#pgm-main'
  } else {
    var p = el.parentElement ;
    var i = [].indexOf.call(p.children, el) + 1 ;
    return selector_path_to_root(p) + ' ' 
      + el.tagName.toLowerCase() 
      + ':nth-child(' + i + ')' ;
  }
}

function pgm_selector_error(selector, message) {
  var el = document.querySelector(selector) ;
  pgm_error(el, message) ;
}

function pgm_error(el, message) {
  Pgm.addClass(el, "error-target") ;
  var error = new Error(message) ;
  throw error ;
}

function pgm_show_error(e) {
  var error_panel = document.getElementById('error-panel') ;
  var error_msg = document.getElementById('error-msg') ;
  Pgm.removeClass(error_panel, 'hidden') ;
  error_msg.innerHTML = e.message ;//+ '<br/>' + e.stack;
}

function pgm_clear_error() {
  var elts = document.getElementsByClassName("error-target") ;
  while( elts.length > 0 ) {
    Pgm.removeClass(elts[0], 'error-target') ;
  }
  var error_panel = document.getElementById('error-panel') ;
  Pgm.addClass(error_panel, 'hidden') ;
}

document.getElementById('error-close').addEventListener(
  'click',
  pgm_clear_error
) ;

var log_elt = document.getElementById('log-msg') ;
var log_panel = document.getElementById('log-panel') ;

function pgm_log(msg) {
  log_elt.innerHTML = log_elt.innerHTML + msg + '<br/>' ;
  Pgm.removeClass(log_panel, 'hidden') ;
}

function pgm_clear_log() {
  log_elt.innerHTML = '' ;
  Pgm.addClass(log_panel, 'hidden') ;
}
document.getElementById('log-close').addEventListener(
  'click',
  pgm_clear_log
) ;

/*}}}*/

/* {{{ Color and direction ================================================== */

var dir_list = [
  "bas",
  "droite",
  "haut",
  "gauche"
] ;

var col_list = [
  "jaune",
  "vert",
  "bleu",
  "rouge"
] ;

function check_direction(str, error_target) {
  if(dir_list.indexOf(str) > -1) {
    return str ;
  } else {
    return pgm_selector_error(
        error_target,
        "direction invalide : " + str
        );
  }
}

function check_color(str, error_target) {
  if(col_list.indexOf(str) > -1) {
    return str ;
  } else {
    return pgm_selector_error(
        error_target,
        "couleur invalide : " + str 
        ) ;
  }
}

function get_movement(str, error_target) {
  if(col_list.indexOf(str) > -1)
    return color_to_number(str) ;
  else if(dir_list.indexOf(str) > -1) {
    return direction_to_number(str) ;
  } else {
    return pgm_selector_error(
        error_target,
        "mouvement invalide : " + str
        ) ;
  }
}

function direction_to_number(dirname) {
  return dir_list.indexOf(dirname) ;
}

function number_to_direction(number) {
  return dir_list[number] ;
}

function map_orientation() {
  var boussole = document.getElementById("boussole") ;
  return boussole.getAttribute("transform") ;
}

function color_to_number(color) {
  var orientation = map_orientation() ;
  var index = col_list.indexOf(color) ;
  index = (index + 4 - parseInt(orientation.match(/\d/))) % 4 ;
  if(orientation.match(/flip/)) {
    index = (4 - index) % 4 ;
  }
  return index ;
}

function number_to_color(number) {
  var orientation = map_orientation() ;
  var index = number ;
  if(orientation.match(/flip/)) {
    index = (4 - index) % 4 ;
  }
  index = (index + parseInt(orientation.match(/\d/))) % 4 ;
  return col_list[index] ;
}

function color_to_direction(color) {
  return number_to_direction(color_to_number(color)) ;
}

function direction_to_color(direction) {
  return number_to_color(direction_to_number(direction)) ;
}

function next_direction(direction) {
  var index = dir_list.indexOf(direction) ;
  return dir_list[(index + 1)%4] ;
}

function next_color(color) {
  var index = col_list.indexOf(color) ;
  return col_list[(index + 1)%4] ;
}

/* Compile */

function command_dir_up(el) {
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(2)' ;
  } else {
    return '"haut"' ;
  }
}

pgm_elements['cmd-dir-up'] = command_dir_up ;

function command_dir_left(el) {
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(3)' ;
  } else {
    return '"gauche"' ;
  }
}

pgm_elements['cmd-dir-left'] = command_dir_left ;

function command_dir_down(el) {
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(0)' ;
  } else {
    return '"bas"' ;
  }
}
pgm_elements['cmd-dir-down'] = command_dir_down ;

function command_dir_right(el) {
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(1)' ;
  } else {
    return '"droite"' ;
  }
}

pgm_elements['cmd-dir-right'] = command_dir_right ;

function command_col_red(el) {
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(color_to_number("rouge"))' ;
  } else {
    return '"rouge"' ;
  }
}

pgm_elements['cmd-col-red'] = command_col_red ;

function command_col_yellow(el) {
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(color_to_number("yellow"))' ;
  } else {
    return '"jaune"' ;
  }
}

pgm_elements['cmd-col-yellow'] = command_col_yellow ;

function command_col_green(el) {
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(color_to_number("vert"))' ;
  } else {
    return '"vert"' ;
  }
}

pgm_elements['cmd-col-green'] = command_col_green ;

function command_col_blue(el) {
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(color_to_number("bleu"))' ;
  } else {
    return '"bleu"' ;
  }
}

pgm_elements['cmd-col-blue'] = command_col_blue ;

function command_col_of(el) {
  var receiver = el.children[1] ;
  var dir = receiver_compile(receiver) ;
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(direction_to_number(' + dir + '))' ;
  } else {
    return 'direction_to_color(' + dir + ')' ;
  }
}

pgm_elements['cmd-col-of'] = command_col_of ;

function command_dir_of(el) {
  var receiver = el.children[1] ;
  var col = receiver_compile(receiver) ;
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(color_to_number(' + col + '))' ;
  } else {
    return 'color_to_direction(' + col + ')' ;
  }
}

pgm_elements['cmd-dir-of'] = command_dir_of ;


function command_next_col(el) {
  var receiver = el.children[1] ;
  var col = receiver_compile(receiver) ;
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go((color_to_number(' + col + ') + 1)%4)' ;
  } else {
    return 'number_to_color((color_to_number(' + col + ') + 1)%4)' ;
  }
}

pgm_elements['cmd-next-col'] = command_next_col ;

function command_next_dir(el) {
  var receiver = el.children[1] ;
  var dir = receiver_compile(receiver) ;
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go((direction_to_number(' + dir + ') + 1)%4)' ;
  } else {
    return 'number_to_direction((direction_to_number(' + dir + ') + 1)%4)' ;
  }
}

pgm_elements['cmd-next-dir'] = command_next_dir ;

/*}}}*/

/* {{{ Color and direction ================================================== */

function command_draw_mv(el) {
  var receiver = el.children[1] ;
  var dir = receiver_compile(receiver) ;
  return 'Explosurf.draw.go(direction_to_number(' + dir + '))' ;
}

pgm_elements['cmd-draw-mv'] = command_draw_mv ;

function command_draw_stamp(el) {
  return 'Explosurf.draw.stamp()' ;
}

pgm_elements['cmd-draw-stamp'] = command_draw_stamp ;

function command_draw_erase(el) {
  return 'Explosurf.draw.clear()' ;
}

pgm_elements['cmd-draw-erase'] = command_draw_erase ;

function command_draw_erase_all(el) {
  return 'Explosurf.draw.clear_all()' ;
}

pgm_elements['cmd-draw-erase-all'] = command_draw_erase_all ;

/*}}}*/

/* {{{ Variable management ================================================== */

function get_variable(name, err_target, global) {
  var data = Explosurf.notes.get_variable(name, global) ;
  if(data !== null) {
    return data ;
  } else {
    return pgm_selector_error(
        err_target,
        "variable inexistante : " + name 
        ) ;
  }
}

function command_write_local(el) {
  var name = receiver_compile(el.children[1]) ;
  var value = receiver_compile(el.children[2]) ;
  value = "typeof " + value + " === 'boolean' ? " 
    + translate_bool(value) + ' : ' 
    + value ;
  return 'Explosurf.notes.set_variable(' + name + ', ' + value + ')' ;
}

pgm_elements['cmd-var-local-set'] = command_write_local ;

function command_read_local(el) {
  var name = receiver_compile(el.children[2]) ;
  var err_target = '"' + selector_path_to_root(el.children[2]) + '"' ;
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(get_movement(get_variable(' 
      + name + ', ' + err_target + '), ' + err_target + '))' ;
  } else {
    return 'get_variable(' + name + ', ' + err_target + ')' ;
  }
}

pgm_elements['cmd-var-local-read'] = command_read_local ;

function command_delete_local(el) {
  var name = receiver_compile(el.children[2]) ;
  return 'Explosurf.notes.clear_variable(' + name + ')' ;
}

pgm_elements['cmd-var-local-delete-var'] = command_delete_local ;

function command_delete_tile_local(el) {
  return 'Explosurf.notes.clear_variable(".*")' ;
}

pgm_elements['cmd-var-local-delete-tile'] = command_delete_tile_local ;

function command_delete_all_local(el) {
  return 'Explosurf.notes.clear_local_variables(".*")' ;
}

pgm_elements['cmd-var-local-delete-world'] = command_delete_all_local ;

function command_write_global(el) {
  var name = receiver_compile(el.children[1]) ;
  var value = receiver_compile(el.children[2]) ;
  value = "typeof " + value + " === 'boolean' ? " 
    + translate_bool(value) + ' : ' 
    + value ;
  return 'Explosurf.notes.set_variable(' + name + ', ' + value + ', true)' ;
}

pgm_elements['cmd-var-global-set'] = command_write_global ;

function command_read_global(el) {
  var name = receiver_compile(el.children[2]) ;
  var err_target = '"' + selector_path_to_root(el.children[2]) + '"' ;
  if(el.parentElement.className.match(/pgm-recv-void/)) {
    return 'Explosurf.explorer.go(get_movement(get_variable(' 
      + name + ', ' + err_target + ', true), ' + err_target + '))' ;
  } else {
    return 'get_variable(' + name + ', ' + err_target + ', true)' ;
  }
}

pgm_elements['cmd-var-global-read'] = command_read_global ;

function command_delete_global(el) {
  var name = receiver_compile(el.children[2]) ;
  return 'Explosurf.notes.clear_variable(' + name + ', true)' ;
}

pgm_elements['cmd-var-global-delete-var'] = command_delete_global ;

function command_delete_all_global(el) {
  return 'Explosurf.notes.clear_variable(".*", true)' ;
}

pgm_elements['cmd-var-global-delete-all'] = command_delete_all_global ;

/*}}}*/

/* {{{ Branching ============================================================ */

function block_if(el) {
  var test = receiver_compile(el.children[0].children[0]) ;
  var block = block_compile(el.children[1]) ;
  return 'if(' + test + '){' + block + '}' ;
}

pgm_elements['cmd-block-if'] = block_if ;

function block_if_else(el) {
  var test = receiver_compile(el.children[0].children[0]) ;
  var blockif = block_compile(el.children[1]) ;
  var blockelse = block_compile(el.children[3]) ;
  return 'if(' + test + '){' + blockif + '} else {' + blockelse + '}' ;
}

pgm_elements['cmd-block-if-else'] = block_if_else ;

var while_index = 0 ;

function block_while(el) {
  var test = receiver_compile(el.children[0].children[0]) ;
  var block = block_compile(el.children[1]) ;
  var err_target = '"' + selector_path_to_root(el) + '"' ;
  var counter_var = 'counter_' + while_index ;
  ++ while_index ;
  var code = '' ;
  code += 'var ' + counter_var + ' = 0 ;' ;
  code += 'for(;' + test + ';++' + counter_var + '){ ' ;
  code += '  if( ' + counter_var + ' > 10000) {' ;
  code += '    pgm_selector_error( ' + err_target + ', "cette boucle fait plus de 10000 itérations ") ;'
  code += '  } ' 
  code +=    block ;
  code += '}' ;
  return code ;
}

pgm_elements['cmd-block-while'] = block_while ;

function block_repeat(el) {
  var iter = receiver_compile(el.children[0].children[0]) ;
  var block = block_compile(el.children[1]) ;
  return 'for( var i = 0; i < ' + iter + '; ++i){' + block + '}' ;
}

pgm_elements['cmd-block-repeat'] = block_repeat ;

/*}}}*/

/* {{{ Boolean expressions ================================================== */

function command_equals(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' == ' + rhs + ')' ;
}

pgm_elements['cmd-test-equal'] = command_equals ;

function command_gt(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' > ' + rhs + ')' ;
}

pgm_elements['cmd-test-gt'] = command_gt ;

function command_ge(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' >= ' + rhs + ')' ;
}

pgm_elements['cmd-test-ge'] = command_ge ;

function command_not(el) {
  var test = receiver_compile(el.children[0]) ;
  return '(! ' + test + ')' ;
}

pgm_elements['cmd-test-not'] = command_not ;

function command_or(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' || ' + rhs + ')' ;
}

pgm_elements['cmd-test-or'] = command_or ;

function command_and(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' && ' + rhs + ')' ;
}

pgm_elements['cmd-test-and'] = command_and ;

function command_exists_local(el) {
  var name = receiver_compile(el.children[1]) ;
  return 'Explosurf.notes.check_variable(' + name + ')' ;
}

pgm_elements['cmd-test-var-exists-local'] = command_exists_local ;

function command_exists_global(el) {
  var name = receiver_compile(el.children[1]) ;
  return 'Explosurf.notes.check_variable(' + name + ', true)' ;
}

pgm_elements['cmd-test-var-exists-global'] = command_exists_global ;

function command_border_dir(el) {
  var direction = receiver_compile(el.children[0]) ;
  return 'Explosurf.explorer.check_boundary(direction_to_number(' + direction + '))' ;
}

pgm_elements['cmd-test-boundary-dir'] = command_border_dir ;

function command_border_col(el) {
  var color = receiver_compile(el.children[0]) ;
  return 'Explosurf.explorer.check_boundary(color_to_number(' + color + '))' ;
}

pgm_elements['cmd-test-boundary-col'] = command_border_col ;

/*}}}*/

/* {{{ Arithmetics ========================================================== */

function command_add(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' + ' + rhs + ')' ;
}

pgm_elements['cmd-arith-plus'] = command_add ;

function command_sub(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' - ' + rhs + ')' ;
}

pgm_elements['cmd-arith-minus'] = command_sub ;

function command_mult(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' * ' + rhs + ')' ;
}

pgm_elements['cmd-arith-mult'] = command_mult ;

function command_div(el) {
  var lhs = receiver_compile(el.children[0]) ;
  var rhs = receiver_compile(el.children[1]) ;
  return '(' + lhs + ' / ' + rhs + ' << 0)' ;
}

pgm_elements['cmd-arith-div'] = command_div ;

/*}}}*/

/* {{{ Strings ============================================================== */

function command_concat(el) {
  var lhs = receiver_compile(el.children[1]) ;
  var rhs = receiver_compile(el.children[2]) ;
  return lhs + ' + ' + rhs ;
}

pgm_elements['cmd-str-concat'] = command_concat ;

function command_log(el) {
  var msg = receiver_compile(el.children[1]) ;
  return 'pgm_log(' + msg + ')' ;
}

pgm_elements['cmd-str-log'] = command_log ;

/*}}}*/

/* {{{ Compilation ========================================================== */

function show_progress() {
  var progress = document.getElementById("progress") ;
  Pgm.removeClass(progress, 'hidden') ;
}

function hide_progress() {
  var progress = document.getElementById("progress") ;
  Pgm.addClass(progress, 'hidden') ;
}

function run() {
  Pgm.save() ;
  pgm_clear_error() ;
  pgm_clear_log() ;
  while_index = 0 ;
  var el = document.getElementById("pgm-main") ;
  show_progress() ;
  //timeout to allow the browser to update and show the bar
  setTimeout(function () {
    try {
      var program = 'try{' + block_compile(el) + '}catch(e){pgm_show_error(e);}' ;
      eval(program) ;
      hide_progress() ;
    } catch (e) {
      pgm_show_error(e) ;
      hide_progress() ;
      throw e ;
    }
    Explosurf.draw.redraw() ;
  }, 100) ;
}

document.getElementById("pgm-run").addEventListener('click', run) ;

/*}}}*/

}) //end of namespace

<div class="col-xs-12">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h2 class="panel-title">Mission spéciale</h2>
    </div>
    <div class="panel-body">
      Warda la girafe 
      <img alt="" src="<?php echo $rootpath?>/Images/warda_la_girafe.png"/> 
      ne sait plus comment revenir à son arbre 
      <img alt="" src="<?php echo $rootpath?>/Images/arbre_de_warda.png"/>. 
      Sauras-tu lui indiquer le chemin ?
    </div>
  </div>
</div>

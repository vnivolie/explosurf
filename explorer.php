<!DOCTYPE html>
<html>
  <head>
    <!--{{{ header-->
    <title>Explosurf</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <?php 
      $path = getcwd() ;
      $base_pattern = '/(.*(Explosurf|explosurf)\/).*/' ;
      $base_replacement = '$1' ;
      $base = preg_replace($base_pattern, $base_replacement, $path) ;
      $pathend = str_replace($base,"",$path) ;
      $rootpath_pattern = '/(\w+)/' ;
      $rootpath_replacement = '..' ;
      $rootpath = preg_replace($rootpath_pattern, $rootpath_replacement, $pathend) ;
    ?>
    <link rel="stylesheet" type="text/css" media="screen" 
      href="<?php echo $rootpath?>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" media="screen" 
      href="<?php echo $rootpath?>/css/style.css" />
	<!--}}}-->	
  </head>
  <body>
    <div class="container">
      <!--{{{ top bar-->
      <div class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" 
              class="navbar-toggle collapsed" 
              data-toggle="collapse" 
              data-target="#navbar" 
              aria-expanded="false" 
              aria-controls="navbar"
              >
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" id='planet-name'>
              <?php 
                  // get the current directory name
                  echo basename(getcwd()) ;
              ?>
            </a>
          </div>
          <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
              <li>
                <label class="btn btn-link navbar-btn btn-file" role="link" id="open-btn">
                  <span class="glyphicon glyphicon-floppy-open"></span>
                  Ouvrir
                  <input type="file" id="file-open" style="display: none;">
                </label>
              </li>
              <li>
                <a href="#" id="save-btn">
                  <span class="glyphicon glyphicon-floppy-save"></span>
                  Sauver
                </a>
              </li>
              <li>
                <a data-toggle="collapse" href="#help-panel">
                  <span class="glyphicon glyphicon-info-sign"></span>
                  Aide
                </a>
              </li>
              <li>
                <a href="<?php echo $rootpath?>/index.html">
                  <span class="glyphicon glyphicon-arrow-left"></span>
                  Retour
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!--}}}-->

      <div class="container-fluid" id="panel-layout">
        <!--{{{ help-->
        <div class="panel panel-default collapse" id="help-panel">
        <!--<div class="panel panel-default" id="help-panel">-->
          <div class="panel-heading">
            <h2 class="panel-title">Aide</h2>
          </div>
          <div class="panel-body text-justify">

            <h3>Déplacements</h3>

            <p> 
            Chaque planète est constituée d'un certain nombre de cases. Vous
            arriverez sur une case quelconque, qui ne sera pas forcément la même
            que celle des autres explorateurs. Vous n'y arriverez pas forcément
            non-plus dans le même sens que les autres. Cependant, si vous quittez
            la planète et y revenez plus tard depuis le même ordinateur, vous
            devriez normalement revenir à l'endroit où vous l'aviez quittée. Vous
            avez la possibilité de vous déplacer de case en case en utilisant les
            flèches indiquées ci-dessous.
            </p>

            <div class="text-center">
              <ul class=example-list>
                <li class="example-arrow-square">
                  <div class="example-arrow arrow_left"></div>
                </li>
                <li class="example-arrow-square">
                  <div class="example-arrow arrow_bottom"></div>
                </li>
                <li class="example-arrow-square">
                  <div class="example-arrow arrow_right"></div>
                </li>
                <li class="example-arrow-square">
                  <div class="example-arrow arrow_top"></div>
                </li>
              </ul>
            </div>


            <h3>Boussole</h3>

            <div class="boussole_fake example"></div>

            <p> Cette boussole un peu particulière pourra se révéler d'une aide
            précieuse. Elle sera toujours orientée de la même façon par rapport à
            une case donnée. Si la case tourne, la boussole tourne avec, comme sur
            l'exemple fourni ci-dessous. Attention, ces boussoles n'indiquent pas 
            de « nord » et ne sont pas forcément orientées de la même façon entre
            deux cases voisines. D'ailleurs, est-il toujours possible de définir
            un « nord » sur une planète ? Chaque case a sa propre boussole. À
            vous ensuite de trouver comment utiliser cet instrument au mieux !
            </p>

            <div class="text-center">
              <ul class="example-list">
                <li>
                  <div class="tile">
                    <img 
                      class="img-responsive img-rounded"
                      alt="Example tile r0"
                      src="<?php echo $rootpath?>/Images/sample_tile_r0.png"
                    >
                    </img>
                    <div class=overlay>
                      <div class="boussole" transform="r0"></div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="tile">
                    <img 
                      class="img-responsive img-rounded"
                      alt="Example tile r1"
                      src="<?php echo $rootpath?>/Images/sample_tile_r1.png"
                    >
                    </img>
                    <div class=overlay>
                      <div class="boussole" transform="r1"></div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="tile">
                    <img 
                      class="img-responsive img-rounded"
                      alt="Example tile r2"
                      src="<?php echo $rootpath?>/Images/sample_tile_r2.png"
                    >
                    </img>
                    <div class=overlay>
                      <div class="boussole" transform="r2"></div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="tile">
                    <img 
                      class="img-responsive img-rounded"
                      alt="Example tile r3"
                      src="<?php echo $rootpath?>/Images/sample_tile_r3.png"
                    >
                    </img>
                    <div class=overlay>
                      <div class="boussole" transform="r3"></div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>

            <h3>Notes</h3>

            <p> Deux blocs notes sont mis à votre disposition. Les notes locales sont
            spécifiques à la case sur laquelle vous vous trouvez. Si vous quittez
            cette case, un bloc note vierge apparaîtra pour la nouvelle case. En
            revenant sur vos pas, vous devriez pouvoir retrouver les notes que vous
            avez laissées sur les cases précédentes. Les notes globales quant à
            elles sont communes à toute la planète.
            </p>

            <h3>Carte</h3>

            <p> La carte vous permet de reproduire à plat les cases du monde. Le
            petit bonhomme symbolise l'endroit où vous dessinez sur la carte.
            Les boutons <span class="glyphicon glyphicon-triangle-left"></span>,
            <span class="glyphicon glyphicon-triangle-bottom"></span>, 
            <span class="glyphicon glyphicon-triangle-top"></span> et 
            <span class="glyphicon glyphicon-triangle-right"></span> vous permettent
            de déplacer le curseur dans la direction correspondante. Le bouton 
            <span class="glyphicon glyphicon-tint"></span>
            permet de reproduire sous le curseur la case centrale du monde. La gomme
            <span class="glyphicon glyphicon-erase"></span> efface la case dessinée
            sous le curseur, et la poubelle 
            <span class="glyphicon glyphicon-trash"></span>
            efface toute la carte. Vous pouvez sauver l'image dessiner en utilisant 
            un clic droit.
            </p>

            <h3>Programme</h3>

            <p>
            Le programme vous permet d'automatiser l'exploration de la planète.
            L'interface sera différentes selon votre type de navigateur : mobile ou poste de travail.
            </p>

            <h4>Interface de bureau</h4>
            
            <p>
            Sur un poste de bureau, vous pourrez ajouter des instructions et les
            réordonner en les faisant glisser. Il ne sera pas possible de
            déposer une instructions à un emplacement incompatible. En appuyant
            sur la touche <kbd>Ctrl</kbd> ou en cliquant sur le bouton 
            copie&nbsp;<span class= "glyphicon glyphicon-copy"></span>, vous pourrez
            dupliquer des instructions au lieu de les réordonner dans votre
            programme. En déplacement des instructions depuis votre programme
            pour les déposer dans la bibliothèque, vous les supprimerez. Si vous
            préférez l'interface par clics, vous pouvez forcer le fonctionnement
            mobile en utilisant le bouton mobile&nbsp;<span class="glyphicon
            glyphicon-phone"></span>.
            </p>

            <h4>Interface mobile</h4>
            
            <p>
            Sur un mobile, vous pouvez ajouter des instructions en touchant
            l'endroit où vous voulez en ajouter. La bibliothèque apparaîtra
            alors pour vous permettre de choisir les instructions. Selon la zone
            touchées, seules les instructions compatibles seront proposées. Vous
            pouvez annuler l'action en utilisant le bouton de fermeture de la
            bibliothèque en haut à droite&nbsp;<span class="glyphicon
            glyphicon-remove"></span>. Une petite barre d'outils en bas du
            programme vous permet de choisir le comportement du clic : 
            ajouter&nbsp;<span class="glyphicon glyphicon-plus"></span>, couper 
            (supprimer)&nbsp;<span class="glyphicon glyphicon-scissors"></span>, 
            copier&nbsp;<span class="glyphicon glyphicon-copy"></span> ou 
            coller&nbsp;<span class="glyphicon glyphicon-paste"></span>.
            </p>

            <h4>Sauvegarde</h4>
            <p>
            Les boutons 
            sauvegarde&nbsp;<span class="glyphicon glyphicon-floppy-save"></span>
            et
            chargement&nbsp;<span class="glyphicon glyphicon-floppy-open"></span>
            du panneau programme vous permettent de sauver vos programmes sous
            différents noms et de les charger. Les programmes sauvegardés sont
            disponibles sur toutes les planètes.
            </p>

            <h4>Bibliothèque</h4>

            <p>
            La bibliothèque regroupe les instructions disponibles dans
            différentes catégories. 
            <ul>
              <li>
                <span class="glyphicon glyphicon-move"></span> 
                La catégorie <strong>déplacements</strong> permet de se déplacer 
                en utilisant soit les directions soit les couleurs de la boussole.
                Il est également possible de convertir les couleurs en directions et
                vice-versa. L'instruction direction / couleur suivante vous permet
                d'énumérer les directions. L'ordre des directions et des couleurs
                est celui de la bibliothèque : haut, gauche, bas, droite, haut, ...
              </li>
              <li>
                <span class="glyphicon glyphicon-pencil"></span> 
                La catégorie <strong>notes</strong> vous permet d'interagir avec les 
                notes via des <em>variables</em>. Le 
                symbole&nbsp;<span class="glyphicon glyphicon-map-marker">
                </span> signifie que vous utilisez les notes locales et le 
                symbole&nbsp;<span class="glyphicon glyphicon-globe"></span> les notes 
                globales. Vous pouvez écrire&nbsp;&larr;, lire&nbsp;<span class="
                glyphicon glyphicon-search"></span> ou supprimer&nbsp;<span class="
                glyphicon glyphicon-trash"></span> des variables. Une variable
                apparaîtra sous la forme <code>[ nom = valeur ]</code> dans les
                notes. L'action de lire une variable peut être placée quasiment
                partout, si la variable contient le bon type de données. Si une
                variable contient une direction ou une couleur, la lire permet
                de réaliser le déplacement. Si elle contient un nombre ou un
                test, vous pouvez lire le contenu d'une variable dans un
                emplacement réclamant un entier ou un test.
              </li>
              <li>
                <span class="glyphicon glyphicon-picture"></span> 
                La catégorie <strong>carte</strong> vous permet de modifier la
                carte. Vous pouvez déplacer votre curseur en utilisant les
                directions de la catégorie déplacements, reproduire la case
                courante du monde, effacer la case sous le curseur ou effacer
                toute la carte.
              </li>
              <li>
                <span class="glyphicon glyphicon-random"></span> 
                La catégorie <strong>contrôle</strong> vous permet de modifier
                le comportement de votre programme en fonction de tests. Vous
                pouvez également trouver des <em>boucles</em> qui vous
                permettent de répéter plusieurs fois une partie des
                instructions.
              </li>
              <li>
                <span class="glyphicon glyphicon-ok"></span> 
                La catégorie <strong>tests</strong> vous permet de réaliser des
                tests : vérifier si deux valeurs sont identiques, les comparer,
                vérifier l'existence de variables, ou le fait qu'une case est au
                bord de la planète. Ces tests peuvent être fournis aux blocs de
                la catégorie contrôle, ou enregistrés dans des variables.
              </li>
              <li>
                <span class="glyphicon glyphicon-plus"></span> 
                La catégorie <strong>arithmétique</strong> permet de réaliser
                des opérations sur des nombres entiers : addition, soustraction,
                multiplication et division. Attention, la division est entière :
                elle vous donne le quotient de la division euclidienne.
              </li>
              <li>
                <span class="glyphicon glyphicon-font"></span> 
                La catégorie <strong>texte</strong> permet de manipuler du texte. 
                Le trombone&nbsp;<span class="glyphicon glyphicon-paperclip"></span>
                vous permet d'accoler deux mots en un seul. Vous pouvez en
                particulier vous en servir pour créer des noms de variables du
                type <code>voisin_droite</code> en accolant la chaîne
                <code>voisin_</code> et la valeur d'une variable contenant une
                direction. 
                Le haut-parleur&nbsp;<span class="glyphicon glyphicon-bullhorn"></span>
                permet au programme de vous afficher des messages d'information,
                utiles pour étudier le comportement de votre programme.
              </li>
            </ul>
            </p>
            
            <h3>Sauvegarde de toutes les données</h3>

            <p>
            Si vous utilisez toujours le même navigateur sur le même appareil
            pour accéder à Explosurf, vous retrouverez vos données. Vous pouvez
            également sauvegarder et recharger l'ensemble de vos données (les
            programmes et les notes en cours sur toutes les planètes ainsi que
            les programmes sauvegardés) en utilisant les boutons correspondants
            sur la barre de menu tout en haut.
            </p>

          </div>
        </div>
        <!--}}}-->
        <div class="row" id="map">
        <!--{{{ map -->
          <div class="col-sm-12 col-md-9">
            <div class="panel panel-default">
              <ul class="Map">
                <li>
                  <div class=square>
                    <a>
                      <div class="empty_tile"></div>
                    </a>
                  </div>
                </li><!--
                --><li>
                  <div class="square">
                    <a href="#" id="go-up">
                      <div class="tile" id="tile_top_container">
                        <img id="tile_top" alt="" src=""/>
                        <div class=overlay>
                          <div class="arrow arrow_top"></div>
                        </div>
                      </div>
                    </a>
                  </div>
                </li><!--
                --><li>
                  <div class="square">
                    <a>
                      <div class="empty_tile"></div>
                    </a>
                  </div>
                </li><!--
                --><li>
                  <div class="square">
                    <a href="#" id="go-left">
                      <div class="tile" id="tile_left_container">
                        <img id="tile_left" alt="" src=""/>
                        <div class=overlay>
                          <div class="arrow arrow_left"></div>
                        </div>
                      </div>
                    </a>
                  </div>
                </li><!--
                --><li>
                  <div class="square">
                    <a>
                      <div class="tile" id="tile_center_container">
                        <img id="tile_center" alt="" src=""/>
                        <div class=overlay>
                          <div class="boussole" id="boussole" transform="r0_flip"></div>
                          <div id="tile_index"></div>
                        </div>
                      </div>
                    </a>
                  </div>
                </li><!--
                --><li>
                  <div class="square">
                    <a href="#" id="go-right">
                      <div class="tile" id="tile_right_container">
                        <img id="tile_right" alt="" src=""/>
                        <div class=overlay>
                          <div class="arrow arrow_right"></div>
                        </div>
                      </div>
                    </a>
                  </div>
                </li><!--
                --><li>
                  <div class="square">
                    <a>
                      <div class="empty_tile"></div>
                    </a>
                  </div>
                </li><!--
                --><li>
                  <div class="square">
                    <a href="#" id="go-down">
                      <div class="tile" id="tile_bottom_container">
                        <img id="tile_bottom" alt="" src=""/>
                        <div class=overlay>
                          <div class="arrow arrow_bottom"></div>
                        </div>
                      </div>
                    </a>
                  </div>
                </li><!--
                --><li>
                  <div class="square">
                    <a>
                      <div class="empty_tile"></div>
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        <!--}}}-->
        <!--{{{ notes -->
          <div class="col-sm-6 col-md-3">
            <div class="panel panel-default" id="local-notes-panel">
              <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">Notes locales</h3>
                <button 
                  class="btn btn-default pull-right" 
                  data-toggle="modal"
                  data-target="#local-delete-modal"
                  >
                  <span class="glyphicon glyphicon-trash"></span>
                </button>
              </div>
              <div class="panel-body">
                <textarea class="note" id="local_notes"></textarea>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="panel panel-default" id="local-notes-panel">
              <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left">Notes globales</h3>
                <button 
                  type="button"
                  class="btn btn-default pull-right" 
                  data-toggle="modal"
                  data-target="#global-delete-modal"
                  >
                  <span class="glyphicon glyphicon-trash"></span>
                </button>
              </div>
              <div class="panel-body">
                <textarea class="note" id="global_notes"></textarea>
              </div>
            </div>
          </div>
        <!--}}}-->
        </div>
        <div class="row" id="draw">
          <!--{{{ draw area-->
          <div class="col-xs-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title pull-left" id="draw-title">
                  <a class="panel-toggle collapsed" href="#draw-container" data-toggle="collapse">
                    <span class="glyphicon collapse-marker"></span>
                    Carte
                    <span class="collapse-hint collapse-hint-open"><small>(cliquer pour ouvrir)</small></span>
                    <span class="collapse-hint collapse-hint-close"><small>(cliquer pour fermer)</small></span>
                  </a>
                </h3>
                <div class="btn-group pull-right" role="group" id="draw-paint-buttons">
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="draw-clear-all"
                    >
                    <span class="glyphicon glyphicon-trash"></span>
                  </button>
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="draw-clear"
                    >
                    <span class="glyphicon glyphicon-erase"></span>
                  </button>
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="draw-stamp"
                    >
                    <span class="glyphicon glyphicon-tint"></span>
                  </button>
                </div>
                <div class="btn-group pull-right" role="group" id="draw-mv-buttons">
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="draw-left"
                    >
                    <span class="glyphicon glyphicon-triangle-left"></span>
                  </button>
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="draw-up"
                    >
                    <span class="glyphicon glyphicon-triangle-top"></span>
                  </button>
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="draw-down"
                    >
                    <span class="glyphicon glyphicon-triangle-bottom"></span>
                  </button>
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="draw-right"
                    >
                    <span class="glyphicon glyphicon-triangle-right"></span>
                  </button>
                </div>
              </div>
              <div class="panel-body collapse" id=draw-container>
                <canvas id="draw-area" width=1920 height=1200></canvas>
              </div>
            </div>
          </div>
          <!--}}}-->
        </div>
        <div class="row row-offcanvas row-offcanvas-left" id="program">
          <!--{{{ programming indicators-->
          <div class="col-xs-12 hidden" id="progress">
            <div class="progress">
              <div 
                class="progress-bar progress-bar-striped active" 
                role="progressbar"
                aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" 
                style="width : 100%"
                >
              </div>
            </div>
          </div>
          <div class="col-xs-12 hidden" id="log-panel">
            <div class="alert alert-info" role="alert">
              <button type="button" class="close" id="log-close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div id="log-msg"></div>
            </div>
          </div>
          <div class="col-xs-12 hidden" id="error-panel">
            <div class="alert alert-danger alert-dismissible" role='alert'>
              <span class="glyphicon glyphicon-exclamation-sign"></span>
              <button type="button" class="close" id="error-close">
                <span aria-hidden="true">&times;</span>
              </button>
              <span id="error-msg"></span>
            </div>
          </div>
          <!--}}}-->
          <!--{{{ commands-->
          <div class="col-xs-12 col-sm-5 col-md-4 sidebar-offcanvas" id="library">
            <div class="panel panel-default" id="library-panel">
              <!--{{{ tabs-->
              <div class="panel-heading tabbed-heading">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a id="tab_mv" class="glyphicon glyphicon-move" data-toggle="tab" href=#mv_cmds></a>
                  </li>
                  <li>
                    <a id="tab_var" class="glyphicon glyphicon-pencil" data-toggle="tab" href=#var_cmds></a>
                  </li>
                  <li>
                    <a id="tab_draw" class="glyphicon glyphicon-picture" data-toggle="tab" href=#draw_cmds></a>
                  </li>
                  <li>
                    <a id="tab_ctrl" class="glyphicon glyphicon-random" data-toggle="tab" href=#ctrl_cmds></a>
                  </li>
                  <li>
                    <a id="tab_test" class="glyphicon glyphicon-ok" data-toggle="tab" href=#test_cmds></a>
                  </li>
                  <li>
                    <a id="tab_arth" class="glyphicon glyphicon-plus" data-toggle="tab" href=#arth_cmds></a>
                  </li>
                  <li>
                    <a id="tab_str" class="glyphicon glyphicon-font" data-toggle="tab" href=#txt_cmds></a>
                  </li>
                </ul>
                <a class="glyphicon glyphicon-remove hidden" 
                  id="btn-close-library" 
                ></a>
              </div>
              <!--}}}-->
              <div class="tab-content panel-collapse collapse in no-top-border-lists" id="library-cmds">
                <!--{{{ movement-->
                <ul id="mv_cmds" class="list-group tab-pane fade library-group in active">
                  <li class="list-group-item pgm-command pgm-type-direction cmd-dir-up">
                    <span class="glyphicon glyphicon-chevron-up"></span>
                    haut
                  </li>
                  <li class="list-group-item pgm-command pgm-type-direction cmd-dir-left">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    gauche 
                  </li>
                  <li class="list-group-item pgm-command pgm-type-direction cmd-dir-down">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                    bas 
                  </li>
                  <li class="list-group-item pgm-command pgm-type-direction cmd-dir-right">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    droite 
                  </li>
                  <li class="list-group-item pgm-command pgm-type-color cmd-col-red">
                    <span class="glyphicon glyphicon-adjust text-danger"></span>
                    rouge 
                  </li>
                  <li class="list-group-item pgm-command pgm-type-color cmd-col-yellow">
                    <span class="glyphicon glyphicon-adjust text-warning"></span>
                    jaune 
                  </li>
                  <li class="list-group-item pgm-command pgm-type-color cmd-col-green">
                    <span class="glyphicon glyphicon-adjust text-success"></span>
                    vert 
                  </li>
                  <li class="list-group-item pgm-command pgm-type-color cmd-col-blue">
                    <span class="glyphicon glyphicon-adjust text-primary"></span>
                    bleu 
                  </li>
                  <li class="list-group-item pgm-command pgm-type-color cmd-col-of">
                    <span class="glyphicon glyphicon-transfer"></span>
                    couleur de 
                    <ul class="pgm-recv pgm-recv-direction">
                      <li class="placeholder">direction</li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-direction cmd-dir-of">
                    <span class="glyphicon glyphicon-transfer"></span>
                    direction de 
                    <ul class="pgm-recv pgm-recv-color">
                      <li class="placeholder">couleur</li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-direction cmd-next-dir">
                    <span class="glyphicon glyphicon-refresh"></span>
                    direction suivante de 
                    <ul class="pgm-recv pgm-recv-direction">
                      <li class="placeholder">direction</li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-color cmd-next-col">
                    <span class="glyphicon glyphicon-refresh"></span>
                    couleur suivante de 
                    <ul class="pgm-recv pgm-recv-color">
                      <li class="placeholder">couleur</li>
                    </ul>
                  </li>
                </ul>
                <!--}}}-->
                <!--{{{ variables-->
                <ul id="var_cmds" class="list-group tab-pane fade library-group">
                  <li class="list-group-item pgm-command pgm-type-void cmd-var-local-set">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="variable" disabled></input> </li>
                    </ul>
                    &larr;
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="valeur" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-read cmd-var-local-read">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <span class="glyphicon glyphicon-search"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="variable" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-var-local-delete-var">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <span class="glyphicon glyphicon-trash"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="variable" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-var-local-delete-tile">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <span class="glyphicon glyphicon-trash"></span>
                    tout sur la case
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-var-local-delete-world">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <span class="glyphicon glyphicon-trash"></span>
                    tout sur toutes les cases
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-var-global-set">
                    <span class="glyphicon glyphicon-globe"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="variable" disabled></input> </li>
                    </ul>
                    &larr;
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="valeur" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-read cmd-var-global-read">
                    <span class="glyphicon glyphicon-globe"></span>
                    <span class="glyphicon glyphicon-search"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="variable" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-var-global-delete-var">
                    <span class="glyphicon glyphicon-globe"></span>
                    <span class="glyphicon glyphicon-trash"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="variable" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-var-global-delete-all">
                    <span class="glyphicon glyphicon-globe"></span>
                    <span class="glyphicon glyphicon-trash"></span>
                    tout
                  </li>
                </ul>
                <!--}}}-->
                <!--{{{ draw-->
                <ul id="draw_cmds" class="list-group tab-pane fade library-group">
                  <li class="list-group-item pgm-command pgm-type-void cmd-draw-mv">
                    <span class="glyphicon glyphicon-record"></span>
                    déplacer le curseur vers 
                    <ul class="pgm-recv pgm-recv-direction">
                      <li class="placeholder">direction</li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-draw-stamp">
                    <span class="glyphicon glyphicon-tint"></span>
                    reproduire la case
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-draw-erase">
                    <span class="glyphicon glyphicon-erase"></span>
                    effacer la carte sous le curseur
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-draw-erase-all">
                    <span class="glyphicon glyphicon-trash"></span>
                    effacer toute la carte
                  </li>
                </ul>
                <!--}}}-->
                <!--{{{ control-->
                <ul id="ctrl_cmds" class="list-group tab-pane fade library-group">
                  <li class="list-group-item panel panel-default pgm-block pgm-type-void cmd-block-if">
                    <div class="panel-heading">
                      Si
                      <ul class="pgm-recv pgm-recv-bool">
                        <li class="placeholder">test</li>
                      </ul>
                    </div>
                    <ul class="list-group pgm-recv-void">
                      <li class="list-group-item pgm-command block-add no-sort hidden">
                        <span class="glyphicon glyphicon-plus"></span>
                      </li>
                    </ul>
                  </li>
                  <li class="list-group-item panel panel-default pgm-block pgm-type-void cmd-block-if-else">
                    <div class="panel-heading">
                      Si
                      <ul class="pgm-recv pgm-recv-bool">
                        <li class="placeholder">test</li>
                      </ul>
                    </div>
                    <ul class="list-group pgm-recv-void">
                      <li class="list-group-item pgm-command block-add no-sort hidden">
                        <span class="glyphicon glyphicon-plus"></span>
                      </li>
                    </ul>
                    <div class="panel-heading">
                      Sinon
                    </div>
                    <ul class="list-group pgm-recv-void">
                      <li class="list-group-item pgm-command block-add no-sort hidden">
                        <span class="glyphicon glyphicon-plus"></span>
                      </li>
                    </ul>
                  </li>
                  <li class="list-group-item panel panel-default pgm-block pgm-type-void cmd-block-while">
                    <div class="panel-heading">
                      Tant que
                      <ul class="pgm-recv pgm-recv-bool">
                        <li class="placeholder">test</li>
                      </ul>
                    </div>
                    <ul class="list-group pgm-recv-void">
                      <li class="list-group-item pgm-command block-add no-sort hidden">
                        <span class="glyphicon glyphicon-plus"></span>
                      </li>
                    </ul>
                  </li>
                  <li class="list-group-item panel panel-default pgm-block pgm-type-void cmd-block-repeat">
                    <div class="panel-heading">
                      Répéter
                      <ul class="pgm-recv pgm-recv-int">
                        <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                      </ul>
                      fois
                    </div>
                    <ul class="list-group pgm-recv-void">
                      <li class="list-group-item pgm-command block-add no-sort hidden">
                        <span class="glyphicon glyphicon-plus"></span>
                      </li>
                    </ul>
                  </li>
                </ul>
                <!--}}}-->
                <!--{{{ tests-->
                <ul id="test_cmds" class="list-group tab-pane fade library-group">
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-equal">
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="valeur" disabled></input> </li>
                    </ul>
                    =
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="valeur" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-gt">
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    &gt;
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-ge">
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    &ge;
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-not">
                    non
                    <ul class="pgm-recv pgm-recv-bool">
                      <li class="placeholder">test</li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-or">
                    <ul class="pgm-recv pgm-recv-bool">
                      <li class="placeholder">test</li>
                    </ul>
                    ou
                    <ul class="pgm-recv pgm-recv-bool">
                      <li class="placeholder">test</li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-and">
                    <ul class="pgm-recv pgm-recv-bool">
                      <li class="placeholder">test</li>
                    </ul>
                    et
                    <ul class="pgm-recv pgm-recv-bool">
                      <li class="placeholder">test</li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-var-exists-local">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="variable" disabled></input> </li>
                    </ul>
                    existe
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-var-exists-global">
                    <span class="glyphicon glyphicon-globe"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="variable" disabled></input> </li>
                    </ul>
                    existe
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-boundary-dir">
                    <ul class="pgm-recv pgm-recv-direction">
                      <li class="placeholder">direction</li>
                    </ul>
                    est un bord
                  </li>
                  <li class="list-group-item pgm-command pgm-type-bool cmd-test-boundary-col">
                    <ul class="pgm-recv pgm-recv-color">
                      <li class="placeholder">couleur</li>
                    </ul>
                    est un bord
                  </li>
                </ul>
                <!--}}}-->
                <!--{{{ arithmetics-->
                <ul id="arth_cmds" class="list-group tab-pane fade library-group">
                  <li class="list-group-item pgm-command pgm-type-int cmd-arith-plus">
                    (
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    +
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    )
                  </li>
                  <li class="list-group-item pgm-command pgm-type-int cmd-arith-minus">
                    (
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    -
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    )
                  </li>
                  <li class="list-group-item pgm-command pgm-type-int cmd-arith-mult">
                    (
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    &times;
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    )
                  </li>
                  <li class="list-group-item pgm-command pgm-type-int cmd-arith-div">
                    (
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    &divide;
                    <ul class="pgm-recv pgm-recv-int">
                      <li class="field"><input autocomplete=off size=8 placeholder="entier" disabled></input> </li>
                    </ul>
                    )
                  </li>
                </ul>
                <!--}}}-->
                <!--{{{ strings-->
                <ul id="txt_cmds" class="list-group tab-pane fade library-group">
                  <li class="list-group-item pgm-command pgm-type-str cmd-str-concat">
                    <span class="glyphicon glyphicon-paperclip"></span>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="debut" disabled></input> </li>
                    </ul>
                    <ul class="pgm-recv pgm-recv-str">
                      <li class="field"><input autocomplete=off size=8 placeholder="fin" disabled></input> </li>
                    </ul>
                  </li>
                  <li class="list-group-item pgm-command pgm-type-void cmd-str-log">
                    <span class="glyphicon glyphicon-bullhorn"></span>
                    <ul class="pgm-recv pgm-recv-msg">
                      <li class="field"><input autocomplete=off size=20 placeholder="message" disabled></input> </li>
                    </ul>
                  </li>
                </ul>
                <!--}}}-->
              </div>
            </div>
          </div>
          <!---}}}-->
          <!--{{{ program-->
          <div class="col-xs-12 col-sm-7 col-md-8" id="pgm-container">
            <div class="panel panel-default pgm" id="pgm">
            <!--{{{ algorithm-->
              <div class="panel-heading">
                <h3 class="panel-title pull-left" id="pgm-title">
                  <a class="panel-toggle" href="#pgm-main,#library-cmds,#phone-toolbar" data-toggle="collapse">
                    <span class="glyphicon collapse-marker"></span>
                    Programme
                    <span class="collapse-hint collapse-hint-open"><small>(cliquer pour ouvrir)</small></span>
                    <span class="collapse-hint collapse-hint-close"><small>(cliquer pour fermer)</small></span>
                  </a>
                </h3>
                <div class="btn-group pull-right" role="group">
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="pgm-save"
                    data-toggle="modal"
                    data-target="#pgm-save-modal"
                    >
                    <span class="glyphicon glyphicon-floppy-save"></span>
                  </button>
                  <button 
                    class="btn btn-default" 
                    type="button" 
                    id="pgm-load"
                    data-toggle="modal"
                    data-target="#pgm-load-modal"
                    >
                    <span class="glyphicon glyphicon-floppy-open"></span>
                  </button>
                  <button class="btn btn-default" type="button" id="pgm-clear">
                    <span class="glyphicon glyphicon-trash"></span>
                  </button>
                  <button class="btn btn-default" type="button" id="pgm-run">
                    <span class="glyphicon glyphicon-play"></span>
                  </button>
                </div>
                <div class="btn-group pull-right hidden-xs" role="group" id="desktop-switches">
                  <button class="btn btn-default" type="button" id="desktop-ui-phone">
                    <span class="glyphicon glyphicon-phone"></span>
                  </button>
                  <button class="btn btn-default" type="button" id="desktop-ui-copy">
                    <span class="glyphicon glyphicon-copy"></span>
                  </button>
                </div>
              </div>
              <ul class="list-group pgm-recv-void collapse in" id="pgm-main">
                <li class="list-group-item pgm-command block-add no-sort hidden">
                  <span class="glyphicon glyphicon-plus"></span>
                </li>
              </ul>
              <!--}}}-->
              <!--{{{ mobile toolbar-->
              <div class="btn-toolbar hidden collapse in" role="toolbar" id="phone-toolbar">
                <div class="btn-group" role="group">
                  <button class="btn btn-default active" autofocus type="button" id="phone-tool-add">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button><!--
                  --><button class="btn btn-default" type="button" id="phone-tool-cut">
                    <span class="glyphicon glyphicon-scissors"></span>
                  </button><!--
                  --><button class="btn btn-default" type="button" id="phone-tool-copy">
                    <span class="glyphicon glyphicon-copy"></span>
                  </button><!--
                  --><button class="btn btn-default" type="button" id="phone-tool-paste">
                    <span class="glyphicon glyphicon-paste"></span>
                  </button>
                </div>
              </div>
              <!--}}}-->
            </div>
          </div>
          <!--}}}-->
        </div>
        <div class="row" id="extra">
          <!--{{{ extra-->
          <?php 
            // get the XML planet descriptor
            if(file_exists('extra.php')) {
              ob_start() ;
              include 'extra.php' ;
              print ob_get_clean() ;
            }
          ?>
          <!--}}}-->
        </div>
    </div>
    <!--{{{ modals-->
    <!--{{{ notes deletion -->
    <div class="modal fade" id="local-delete-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button 
              type="button" 
              class="close" 
              data-dismiss="modal" 
              aria-label="Close"
              >
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Suppression des notes locales</h4>
          </div>
          <div class="modal-body">
            Confirmez-vous la suppression de toutes les notes locales sur toutes les cases ?
          </div>
          <div class="modal-footer">
            <button 
              type="button" 
              class="btn btn-default" 
              data-dismiss="modal"
              >
              Annuler
            </button>
            <button 
              type="button" 
              class="btn btn-primary" 
              data-dismiss="modal" 
              id="notes-local-delete"
              >
              Confirmer
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="global-delete-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button 
              type="button" 
              class="close" 
              data-dismiss="modal" 
              aria-label="Close"
              >
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Suppression des notes globales</h4>
          </div>
          <div class="modal-body">
            Confirmez-vous la suppression de toutes les notes globales ?
          </div>
          <div class="modal-footer">
            <button 
              type="button" 
              class="btn btn-default" 
              data-dismiss="modal"
              >
              Annuler
            </button>
            <button 
              type="button" 
              class="btn btn-primary" 
              data-dismiss="modal" 
              id="notes-global-delete"
              >
              Confirmer
            </button>
          </div>
        </div>
      </div>
    </div>
    <!--}}}-->
    <!--{{{ program saving-->
    <div class="modal fade" id="pgm-save-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button 
              type="button" 
              class="close" 
              data-dismiss="modal" 
              aria-label="Close"
              >
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Enregistrement du programme</h4>
          </div>
          <div class="modal-body">
            <div class="input-group">
              <span class="input-group-addon">Nom</span>
              <input type="text" placeholder="programme" class="form-control" id="pgm-save-name"></input>
            </div>
          </div>
          <div class="modal-footer">
            <button 
              type="button" 
              class="btn btn-default" 
              data-dismiss="modal"
              >
              Annuler
            </button>
            <button 
              type="button" 
              class="btn btn-primary" 
              data-dismiss="modal" 
              id="pgm-save-local"
              >
              Enregistrer
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal" id="pgm-load-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button 
              type="button" 
              class="close" 
              data-dismiss="modal" 
              aria-label="Close"
              >
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Chargement d'un programme</h4>
          </div>
          <div class="modal-body">
            <div class="list-group" id="pgm-load-candidates">
            </div>
            <div class="panel panel-warning" id="pgm-no-pgm">
              <div class="panel-body bg-warning">
                Aucun programme trouvé
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button 
              type="button" 
              class="btn btn-default" 
              data-dismiss="modal"
              >
              Annuler
            </button>
            <button 
              type="button" 
              class="btn btn-primary" 
              data-dismiss="modal" 
              id="pgm-load-local"
              >
              Charger
            </button>
          </div>
        </div>
      </div>
    </div>
    <!--}}}-->
    <!--}}}-->
    <xml id="planet-xml">
      <?php 
      // get the XML planet descriptor
      $search = glob("*.xml") ;
      $len = count($search) ;
      if($len > 0) {
        $file = fopen($search[0], 'r') ;
        fgets($file) ;
        fpassthru($file) ;
      } else {
        echo "" ;
      }
      ?>
    </xml>
    <script type="text/javascript" src="<?php echo $rootpath?>/js/bootstrap-native.js"></script>
    <script type="text/javascript" src="<?php echo $rootpath?>/js/sortable.js"></script>
    <script type="text/javascript" src="<?php echo $rootpath?>/js/filesaver.js"></script>
    <script type="text/javascript" src="<?php echo $rootpath?>/js/pgm_construction.js"></script>
    <script type="text/javascript" src="<?php echo $rootpath?>/js/pgm_compiler.js"></script>
    <script type="text/javascript" src="<?php echo $rootpath?>/js/notes.js"></script>
    <script type="text/javascript" src="<?php echo $rootpath?>/js/explorer.js"></script>
    <script type="text/javascript" src="<?php echo $rootpath?>/js/draw.js"></script>
    <div class="visible-xs-block" id="mobile-tester"></div>
  </body>
</html>
